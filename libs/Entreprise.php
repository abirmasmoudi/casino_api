<?php 

	include_once './libs/Localisation.php';
	
	class Entreprise
	{
		private static $_instance = null;
		private $login;
		private $password;
		private $realm;
		private $TokenId;
		private $MerchantId;
		private $MerchantSiteId;
		private $Localisation;
		private $Key;

		public static function getInstance(){
			if (is_null(self::$_instance)) {
				self::$_instance = new Entreprise();
			}
			return self::$_instance;
		}

		public static function setInstance($instance){
			self::$_instance = $instance;
		}

		private function __construct()
		{
		}
		
		public function exposeData()
		{
			if (is_object($this)){
				$tmp = get_object_vars($this);
			}

			if (is_array($tmp)) {
				$new = array();
				foreach ($tmp as $key => $value) {
					if (is_object($value)) {
						$new[$key] = $value->exposeData();
					}
					else{
						$new[$key] = $value;
					}
				}
				return $new;
			}
			else{
				return $tmp;
			}
		}

	    /**
	     * @return mixed
	     */
	    public function getLogin()
	    {
	        return $this->login;
	    }

	    /**
	     * @param mixed $login
	     *
	     * @return self
	     */
	    public function setLogin($login)
	    {
	        $this->login = $login;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPassword()
	    {
	        return $this->password;
	    }

	    /**
	     * @param mixed $password
	     *
	     * @return self
	     */
	    public function setPassword($password)
	    {
	        $this->password = $password;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRealm()
	    {
	        return $this->realm;
	    }

	    /**
	     * @param mixed $realm
	     *
	     * @return self
	     */
	    public function setRealm($realm)
	    {
	        $this->realm = $realm;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTokenId()
	    {
	        return $this->TokenId;
	    }

	    /**
	     * @param mixed $TokenId
	     *
	     * @return self
	     */
	    public function setTokenId($TokenId)
	    {
	        $this->TokenId = $TokenId;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantId()
	    {
	        return $this->MerchantId;
	    }

	    /**
	     * @param mixed $MerchantId
	     *
	     * @return self
	     */
	    public function setMerchantId($MerchantId)
	    {
	        $this->MerchantId = $MerchantId;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantSiteId()
	    {
	        return $this->MerchantSiteId;
	    }

	    /**
	     * @param mixed $MerchantSiteId
	     *
	     * @return self
	     */
	    public function setMerchantSiteId($MerchantSiteId)
	    {
	        $this->MerchantSiteId = $MerchantSiteId;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getLocalisation()
	    {
	        return $this->Localisation;
	    }

	    /**
	     * @param mixed $Localisation
	     *
	     * @return self
	     */
	    public function setLocalisation($Localisation)
	    {
	        $this->Localisation = $Localisation;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getKey()
	    {
	        return $this->Key;
	    }

	    /**
	     * @param mixed $Localisation
	     *
	     * @return self
	     */
	    public function setKey($Key)
	    {
	        $this->Key = $Key;

	        return $this;
	    }
	}
?>