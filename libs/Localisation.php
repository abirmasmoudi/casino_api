<?php 
	/**
	* 
	*/
	class Localisation
	{
		private $Country;
		private $Currency;
		private $DecimalPosition;
		private $Language;


		/**
		 * Class Constructor
		 * @param    $Country   
		 * @param    $Currency   
		 * @param    $DecimalPosition   
		 * @param    $Language   
		 */
		public function __construct($Country = "FR", $Currency = "EUR", $DecimalPosition = "2", $Language = "FR")
		{
			$this->Country = $Country;
			$this->Currency = $Currency;
			$this->DecimalPosition = $DecimalPosition;
			$this->Language = $Language;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
	
	    /**
	     * @return mixed
	     */
	    public function getCountry()
	    {
	        return $this->Country;
	    }

	    /**
	     * @param mixed $Country
	     *
	     * @return self
	     */
	    public function setCountry($Country)
	    {
	        $this->Country = $Country;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCurrency()
	    {
	        return $this->Currency;
	    }

	    /**
	     * @param mixed $Currency
	     *
	     * @return self
	     */
	    public function setCurrency($Currency)
	    {
	        $this->Currency = $Currency;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getDecimalPosition()
	    {
	        return $this->DecimalPosition;
	    }

	    /**
	     * @param mixed $DecimalPosition
	     *
	     * @return self
	     */
	    public function setDecimalPosition($DecimalPosition)
	    {
	        $this->DecimalPosition = $DecimalPosition;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getLanguage()
	    {
	        return $this->Language;
	    }

	    /**
	     * @param mixed $Language
	     *
	     * @return self
	     */
	    public function setLanguage($Language)
	    {
	        $this->Language = $Language;

	        return $this;
	    }
	}
?>