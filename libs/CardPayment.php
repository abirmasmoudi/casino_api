<?php
	/**
	* 
	*/
	class CardPayment
	{
		private $PaymentOptionRef;
		private $CustomerRef;
		private $OrderDate;
		private $OrderRef;
		private $OrderTag;
		private $ScoringToken;
		private $InvoiceId;
		private $TotalAmount;
		private $PaymentCardData;
		private $NotificationURL;
		private $AllowCardStorage;
		private $PaymentStoredCardData;


		/**
		 * Class Constructor
		 * @param    $PaymentOptionRef   
		 * @param    $CustomerRef   
		 * @param    $OrderDate   
		 * @param    $OrderRef   
		 * @param    $OrderTag   
		 * @param    $ScoringToken   
		 * @param    $InvoiceId   
		 * @param    $TotalAmount   
		 * @param    $PaymentCardData   
		 * @param    $NotificationURL   
		 * @param    $AllowCardStorage   
		 * @param    $PaymentStoredCardData   
		 */
		public function __construct($PaymentOptionRef, $CustomerRef, $OrderDate, $OrderRef, $OrderTag, $ScoringToken, $InvoiceId, $TotalAmount, $PaymentCardData, $NotificationURL, $AllowCardStorage, $PaymentStoredCardData)
		{
			$this->PaymentOptionRef = $PaymentOptionRef;
			$this->CustomerRef = $CustomerRef;
			$this->OrderDate = $OrderDate;
			$this->OrderRef = $OrderRef;
			$this->OrderTag = $OrderTag;
			$this->ScoringToken = $ScoringToken;
			$this->InvoiceId = $InvoiceId;
			$this->TotalAmount = $TotalAmount;
			$this->PaymentCardData = $PaymentCardData;
			$this->NotificationURL = $NotificationURL;
			$this->AllowCardStorage = $AllowCardStorage;
			$this->PaymentStoredCardData = $PaymentStoredCardData;
		}
		
		public function exposeData()
		{
			if (is_object($this)){
				$tmp = get_object_vars($this);
			}

			if (is_array($tmp)) {
				$new = array();
				foreach ($tmp as $key => $value) {
					if (is_object($value)) {
						$new[$key] = $value->exposeData();
					}
					else{
						$new[$key] = $value;
					}
				}
				return $new;
			}
			else{
				return $tmp;
			}
		}
		
	    /**
	     * @return mixed
	     */
	    public function getPaymentOptionRef()
	    {
	        return $this->PaymentOptionRef;
	    }

	    /**
	     * @param mixed $PaymentOptionRef
	     *
	     * @return self
	     */
	    public function setPaymentOptionRef($PaymentOptionRef)
	    {
	        $this->PaymentOptionRef = $PaymentOptionRef;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCustomerRef()
	    {
	        return $this->CustomerRef;
	    }

	    /**
	     * @param mixed $CustomerRef
	     *
	     * @return self
	     */
	    public function setCustomerRef($CustomerRef)
	    {
	        $this->CustomerRef = $CustomerRef;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOrderDate()
	    {
	        return $this->OrderDate;
	    }

	    /**
	     * @param mixed $OrderDate
	     *
	     * @return self
	     */
	    public function setOrderDate($OrderDate)
	    {
	        $this->OrderDate = $OrderDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOrderRef()
	    {
	        return $this->OrderRef;
	    }

	    /**
	     * @param mixed $OrderRef
	     *
	     * @return self
	     */
	    public function setOrderRef($OrderRef)
	    {
	        $this->OrderRef = $OrderRef;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOrderTag()
	    {
	        return $this->OrderTag;
	    }

	    /**
	     * @param mixed $OrderTag
	     *
	     * @return self
	     */
	    public function setOrderTag($OrderTag)
	    {
	        $this->OrderTag = $OrderTag;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getScoringToken()
	    {
	        return $this->ScoringToken;
	    }

	    /**
	     * @param mixed $ScoringToken
	     *
	     * @return self
	     */
	    public function setScoringToken($ScoringToken)
	    {
	        $this->ScoringToken = $ScoringToken;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getInvoiceId()
	    {
	        return $this->InvoiceId;
	    }

	    /**
	     * @param mixed $InvoiceId
	     *
	     * @return self
	     */
	    public function setInvoiceId($InvoiceId)
	    {
	        $this->InvoiceId = $InvoiceId;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTotalAmount()
	    {
	        return $this->TotalAmount;
	    }

	    /**
	     * @param mixed $TotalAmount
	     *
	     * @return self
	     */
	    public function setTotalAmount($TotalAmount)
	    {
	        $this->TotalAmount = $TotalAmount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentCardData()
	    {
	        return $this->PaymentCardData;
	    }

	    /**
	     * @param mixed $PaymentCardData
	     *
	     * @return self
	     */
	    public function setPaymentCardData($PaymentCardData)
	    {
	        $this->PaymentCardData = $PaymentCardData;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getNotificationURL()
	    {
	        return $this->NotificationURL;
	    }

	    /**
	     * @param mixed $NotificationURL
	     *
	     * @return self
	     */
	    public function setNotificationURL($NotificationURL)
	    {
	        $this->NotificationURL = $NotificationURL;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAllowCardStorage()
	    {
	        return $this->AllowCardStorage;
	    }

	    /**
	     * @param mixed $AllowCardStorage
	     *
	     * @return self
	     */
	    public function setAllowCardStorage($AllowCardStorage)
	    {
	        $this->AllowCardStorage = $AllowCardStorage;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentStoredCardData()
	    {
	        return $this->PaymentStoredCardData;
	    }

	    /**
	     * @param mixed $PaymentStoredCardData
	     *
	     * @return self
	     */
	    public function setPaymentStoredCardData($PaymentStoredCardData)
	    {
	        $this->PaymentStoredCardData = $PaymentStoredCardData;

	        return $this;
	    }
	}
?>