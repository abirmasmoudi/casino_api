<?php 
	/**
	* 
	*/
	class TravellerPassport
	{
		private $ExpirationDate;
		private $IssuanceCountry;


		/**
		 * Class Constructor
		 * @param    $ExpirationDate   
		 * @param    $IssuanceCountry   
		 */
		public function __construct($ExpirationDate, $IssuanceCountry)
		{
			$this->ExpirationDate = $ExpirationDate;
			$this->IssuanceCountry = $IssuanceCountry;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getExpirationDate()
	    {
	        return $this->ExpirationDate;
	    }

	    /**
	     * @param mixed $ExpirationDate
	     *
	     * @return self
	     */
	    public function setExpirationDate($ExpirationDate)
	    {
	        $this->ExpirationDate = $ExpirationDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getIssuanceCountry()
	    {
	        return $this->IssuanceCountry;
	    }

	    /**
	     * @param mixed $IssuanceCountry
	     *
	     * @return self
	     */
	    public function setIssuanceCountry($IssuanceCountry)
	    {
	        $this->IssuanceCountry = $IssuanceCountry;

	        return $this;
	    }
	}
?>