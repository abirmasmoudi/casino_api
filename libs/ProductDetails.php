<?php
	/**
	* 
	*/
	class ProductDetails
	{
		private $Categorie1;
		private $Categorie2;
		private $Categorie3;


		/**
		 * Class Constructor
		 * @param    $Categorie1   
		 * @param    $Categorie2   
		 * @param    $Categorie3   
		 */
		public function __construct($Categorie1, $Categorie2, $Categorie3)
		{
			$this->Categorie1 = $Categorie1;
			$this->Categorie2 = $Categorie2;
			$this->Categorie3 = $Categorie3;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getCategorie1()
	    {
	        return $this->Categorie1;
	    }

	    /**
	     * @param mixed $Categorie1
	     *
	     * @return self
	     */
	    public function setCategorie1($Categorie1)
	    {
	        $this->Categorie1 = $Categorie1;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCategorie2()
	    {
	        return $this->Categorie2;
	    }

	    /**
	     * @param mixed $Categorie2
	     *
	     * @return self
	     */
	    public function setCategorie2($Categorie2)
	    {
	        $this->Categorie2 = $Categorie2;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCategorie3()
	    {
	        return $this->Categorie3;
	    }

	    /**
	     * @param mixed $Categorie3
	     *
	     * @return self
	     */
	    public function setCategorie3($Categorie3)
	    {
	        $this->Categorie3 = $Categorie3;

	        return $this;
	    }
	}
?>