<?php

	/**
	 * 
	 */
	class ShippingDetails
	{
		private $Adress1;
        private $Adress2;
        private $AdressCity;
        private $AdressCountry;
        private $AdressZip;


		/**
		 * Class Constructor
		 * @param    $Adress1   
		 * @param    $Adress2   
		 * @param    $AdressCity   
		 * @param    $AdressCountry   
		 * @param    $AdressZip   
		 */
		public function __construct($Adress1, $Adress2, $AdressCity, $AdressCountry, $AdressZip)
		{
			$this->Adress1 = $Adress1;
			$this->Adress2 = $Adress2;
			$this->AdressCity = $AdressCity;
			$this->AdressCountry = $AdressCountry;
			$this->AdressZip = $AdressZip;
		}
		
	    /**
	     * @return mixed
	     */
	    public function getAdress1()
	    {
	        return $this->Adress1;
	    }

	    /**
	     * @param mixed $Adress1
	     *
	     * @return self
	     */
	    public function setAdress1($Adress1)
	    {
	        $this->Adress1 = $Adress1;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAdress2()
	    {
	        return $this->Adress2;
	    }

	    /**
	     * @param mixed $Adress2
	     *
	     * @return self
	     */
	    public function setAdress2($Adress2)
	    {
	        $this->Adress2 = $Adress2;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAdressCity()
	    {
	        return $this->AdressCity;
	    }

	    /**
	     * @param mixed $AdressCity
	     *
	     * @return self
	     */
	    public function setAdressCity($AdressCity)
	    {
	        $this->AdressCity = $AdressCity;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAdressCountry()
	    {
	        return $this->AdressCountry;
	    }

	    /**
	     * @param mixed $AdressCountry
	     *
	     * @return self
	     */
	    public function setAdressCountry($AdressCountry)
	    {
	        $this->AdressCountry = $AdressCountry;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAdressZip()
	    {
	        return $this->AdressZip;
	    }

	    /**
	     * @param mixed $AdressZip
	     *
	     * @return self
	     */
	    public function setAdressZip($AdressZip)
	    {
	        $this->AdressZip = $AdressZip;

	        return $this;
	    }
	}

?>