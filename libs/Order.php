<?php 
	/**
	* 
	*/
	class Order
	{
		private $OrderDate;
		private $SaleChannel;
		private $ShippingMethod;
		private $ShoppingCartItemCount;
		private $ShoppingCartRef;
		private $TotalAmount;
		private $PrescoreRequestId;
		private $ScheduleEnabled;
		private $TravelDetails;
		private $StayDetails;
		private $ProductDetails;
        private $ShippingDetails;


		/**
		 * Class Constructor
		 * @param    $OrderDate   
		 * @param    $SaleChannel   
		 * @param    $ShippingMethod   
		 * @param    $ShoppingCartItemCount   
		 * @param    $ShoppingCartRef   
		 * @param    $TotalAmount   
		 * @param    $PrescoreRequestId   
		 * @param    $ScheduleEnabled   
		 */
		public function __construct($OrderDate, $SaleChannel, $ShippingMethod, $ShoppingCartItemCount, $ShoppingCartRef, $TotalAmount, $PrescoreRequestId, $ScheduleEnabled)
		{
			$this->OrderDate = $OrderDate;
			$this->SaleChannel = $SaleChannel;
			$this->ShippingMethod = $ShippingMethod;
			$this->ShoppingCartItemCount = $ShoppingCartItemCount;
			$this->ShoppingCartRef = $ShoppingCartRef;
			$this->TotalAmount = $TotalAmount;
			$this->PrescoreRequestId = $PrescoreRequestId;
			$this->ScheduleEnabled = $ScheduleEnabled;
		}
	
        public function exposeData()
        {
            if (is_object($this)){
                $tmp = get_object_vars($this);
            }

            if (is_array($tmp)) {
                $new = array();
                foreach ($tmp as $key => $value) {
                    if (is_object($value)) {
                        $new[$key] = $value->exposeData();
                    }
                    else{
                        $new[$key] = $value;
                    }
                }
                return $new;
            }
            else{
                return $tmp;
            }
        }

        /**
         * @return mixed
         */
        public function getOrderDate()
        {
            return $this->OrderDate;
        }

        /**
         * @param mixed $OrderDate
         *
         * @return self
         */
        public function setOrderDate($OrderDate)
        {
            $this->OrderDate = $OrderDate;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getSaleChannel()
        {
            return $this->SaleChannel;
        }

        /**
         * @param mixed $SaleChannel
         *
         * @return self
         */
        public function setSaleChannel($SaleChannel)
        {
            $this->SaleChannel = $SaleChannel;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getShippingMethod()
        {
            return $this->ShippingMethod;
        }

        /**
         * @param mixed $ShippingMethod
         *
         * @return self
         */
        public function setShippingMethod($ShippingMethod)
        {
            $this->ShippingMethod = $ShippingMethod;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getShoppingCartItemCount()
        {
            return $this->ShoppingCartItemCount;
        }

        /**
         * @param mixed $ShoppingCartItemCount
         *
         * @return self
         */
        public function setShoppingCartItemCount($ShoppingCartItemCount)
        {
            $this->ShoppingCartItemCount = $ShoppingCartItemCount;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getShoppingCartRef()
        {
            return $this->ShoppingCartRef;
        }

        /**
         * @param mixed $ShoppingCartRef
         *
         * @return self
         */
        public function setShoppingCartRef($ShoppingCartRef)
        {
            $this->ShoppingCartRef = $ShoppingCartRef;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getTotalAmount()
        {
            return $this->TotalAmount;
        }

        /**
         * @param mixed $TotalAmount
         *
         * @return self
         */
        public function setTotalAmount($TotalAmount)
        {
            $this->TotalAmount = $TotalAmount;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getPrescoreRequestId()
        {
            return $this->PrescoreRequestId;
        }

        /**
         * @param mixed $PrescoreRequestId
         *
         * @return self
         */
        public function setPrescoreRequestId($PrescoreRequestId)
        {
            $this->PrescoreRequestId = $PrescoreRequestId;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getScheduleEnabled()
        {
            return $this->ScheduleEnabled;
        }

        /**
         * @param mixed $ScheduleEnabled
         *
         * @return self
         */
        public function setScheduleEnabled($ScheduleEnabled)
        {
            $this->ScheduleEnabled = $ScheduleEnabled;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getTravelDetails()
        {
            return $this->TravelDetails;
        }

        /**
         * @param mixed $TravelDetails
         *
         * @return self
         */
        public function setTravelDetails($TravelDetails)
        {
            $this->TravelDetails = $TravelDetails;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getStayDetails()
        {
            return $this->StayDetails;
        }

        /**
         * @param mixed $StayDetails
         *
         * @return self
         */
        public function setStayDetails($StayDetails)
        {
            $this->StayDetails = $StayDetails;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getProductDetails()
        {
            return $this->ProductDetails;
        }

        /**
         * @param mixed $ProductDetails
         *
         * @return self
         */
        public function setProductDetails($ProductDetails)
        {
            $this->ProductDetails = $ProductDetails;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getShippingDetails()
        {
            return $this->ShippingDetails;
        }

        /**
         * @param mixed $ShippingDetails
         *
         * @return self
         */
        public function setShippingDetails($ShippingDetails)
        {
            $this->ShippingDetails = $ShippingDetails;

            return $this;
        }
    }
?>