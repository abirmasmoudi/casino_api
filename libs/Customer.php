<?php
	/**
	* 
	*/
	class Customer
	{
		private static $_instance = null;
		private $CustomerRef;
		private $LastName;
		private $Firstname;
		private $Civility;
		private $MaidenName;
		private $BirthDate;
		private $BirthZipCode;
		private $PhoneNumber;
		private $CellPhoneNumber;
		private $Email;
		private $Address1;
		private $Address2;
		private $Address3;
		private $Address4;
		private $ZipCode;
		private $City;
		private $Country;
		private $Nationality;
		private $IpAddress;
		private $WhiteList;
		private $Order;
		private $History;
		private $Card;

		
		public static function getInstance(){
			if (is_null(self::$_instance)) {
				self::$_instance = new Customer();
			}
			return self::$_instance;
		}

		public static function setInstance($instance){
			self::$_instance = $instance;
		}

		private function __construct()
		{
		}
		
		public function exposeData()
		{
			if (is_object($this)){
				$tmp = get_object_vars($this);
			}

			if (is_array($tmp)) {
				$new = array();
				foreach ($tmp as $key => $value) {
					if (is_object($value)) {
						$new[$key] = $value->exposeData();
					}
					else{
						$new[$key] = $value;
					}
				}
				return $new;
			}
			else{
				return $tmp;
			}
		}
		
	    /**
	     * @return mixed
	     */
	    public function getCustomerRef()
	    {
	        return $this->CustomerRef;
	    }

	    /**
	     * @param mixed $CustomerRef
	     *
	     * @return self
	     */
	    public function setCustomerRef($CustomerRef)
	    {
	        $this->CustomerRef = $CustomerRef;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getLastName()
	    {
	        return $this->LastName;
	    }

	    /**
	     * @param mixed $LastName
	     *
	     * @return self
	     */
	    public function setLastName($LastName)
	    {
	        $this->LastName = $LastName;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getFirstname()
	    {
	        return $this->Firstname;
	    }

	    /**
	     * @param mixed $Firstname
	     *
	     * @return self
	     */
	    public function setFirstname($Firstname)
	    {
	        $this->Firstname = $Firstname;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCivility()
	    {
	        return $this->Civility;
	    }

	    /**
	     * @param mixed $Civility
	     *
	     * @return self
	     */
	    public function setCivility($Civility)
	    {
	        $this->Civility = $Civility;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMaidenName()
	    {
	        return $this->MaidenName;
	    }

	    /**
	     * @param mixed $MaidenName
	     *
	     * @return self
	     */
	    public function setMaidenName($MaidenName)
	    {
	        $this->MaidenName = $MaidenName;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getBirthDate()
	    {
	        return $this->BirthDate;
	    }

	    /**
	     * @param mixed $BirthDate
	     *
	     * @return self
	     */
	    public function setBirthDate($BirthDate)
	    {
	        $this->BirthDate = $BirthDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getBirthZipCode()
	    {
	        return $this->BirthZipCode;
	    }

	    /**
	     * @param mixed $BirthZipCode
	     *
	     * @return self
	     */
	    public function setBirthZipCode($BirthZipCode)
	    {
	        $this->BirthZipCode = $BirthZipCode;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPhoneNumber()
	    {
	        return $this->PhoneNumber;
	    }

	    /**
	     * @param mixed $PhoneNumber
	     *
	     * @return self
	     */
	    public function setPhoneNumber($PhoneNumber)
	    {
	        $this->PhoneNumber = $PhoneNumber;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCellPhoneNumber()
	    {
	        return $this->CellPhoneNumber;
	    }

	    /**
	     * @param mixed $CellPhoneNumber
	     *
	     * @return self
	     */
	    public function setCellPhoneNumber($CellPhoneNumber)
	    {
	        $this->CellPhoneNumber = $CellPhoneNumber;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getEmail()
	    {
	        return $this->Email;
	    }

	    /**
	     * @param mixed $Email
	     *
	     * @return self
	     */
	    public function setEmail($Email)
	    {
	        $this->Email = $Email;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAddress1()
	    {
	        return $this->Address1;
	    }

	    /**
	     * @param mixed $Address1
	     *
	     * @return self
	     */
	    public function setAddress1($Address1)
	    {
	        $this->Address1 = $Address1;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAddress2()
	    {
	        return $this->Address2;
	    }

	    /**
	     * @param mixed $Address2
	     *
	     * @return self
	     */
	    public function setAddress2($Address2)
	    {
	        $this->Address2 = $Address2;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAddress3()
	    {
	        return $this->Address3;
	    }

	    /**
	     * @param mixed $Address3
	     *
	     * @return self
	     */
	    public function setAddress3($Address3)
	    {
	        $this->Address3 = $Address3;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAddress4()
	    {
	        return $this->Address4;
	    }

	    /**
	     * @param mixed $Address4
	     *
	     * @return self
	     */
	    public function setAddress4($Address4)
	    {
	        $this->Address4 = $Address4;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getZipCode()
	    {
	        return $this->ZipCode;
	    }

	    /**
	     * @param mixed $ZipCode
	     *
	     * @return self
	     */
	    public function setZipCode($ZipCode)
	    {
	        $this->ZipCode = $ZipCode;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCity()
	    {
	        return $this->City;
	    }

	    /**
	     * @param mixed $City
	     *
	     * @return self
	     */
	    public function setCity($City)
	    {
	        $this->City = $City;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCountry()
	    {
	        return $this->Country;
	    }

	    /**
	     * @param mixed $Country
	     *
	     * @return self
	     */
	    public function setCountry($Country)
	    {
	        $this->Country = $Country;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getNationality()
	    {
	        return $this->Nationality;
	    }

	    /**
	     * @param mixed $Nationality
	     *
	     * @return self
	     */
	    public function setNationality($Nationality)
	    {
	        $this->Nationality = $Nationality;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getIpAddress()
	    {
	        return $this->IpAddress;
	    }

	    /**
	     * @param mixed $IpAddress
	     *
	     * @return self
	     */
	    public function setIpAddress($IpAddress)
	    {
	        $this->IpAddress = $IpAddress;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getWhiteList()
	    {
	        return $this->WhiteList;
	    }

	    /**
	     * @param mixed $WhiteList
	     *
	     * @return self
	     */
	    public function setWhiteList($WhiteList)
	    {
	        $this->WhiteList = $WhiteList;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOrder()
	    {
	        return $this->Order;
	    }

	    /**
	     * @param mixed $Order
	     *
	     * @return self
	     */
	    public function setOrder($Order)
	    {
	        $this->Order = $Order;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getHistory()
	    {
	        return $this->History;
	    }

	    /**
	     * @param mixed $History
	     *
	     * @return self
	     */
	    public function setHistory($History)
	    {
	        $this->History = $History;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCard()
	    {
	        return $this->Card;
	    }

	    /**
	     * @param mixed $History
	     *
	     * @return self
	     */
	    public function setCard($Card)
	    {
	        $this->Card = $Card;

	        return $this;
	    }
	}
?>