<?php
	/**
	* 
	*/
	abstract class Util
	{
		public static function object_to_array($d) {
			if (is_object($d)){
				$d = get_object_vars($d);
			}

			return is_array($d) ? array_map(__METHOD__, $d) : $d;
		}

		public static function array_to_object($d) {
			return is_array($d) ? (object) array_map(__METHOD__, $d) : $d;
		}
	}
?>