<?php 
/**
* 
*/
	class MerchantCustomerHistory
	{
		private $CanceledOrderAmount;
		private $CanceledOrderCount;
		private $FirstOrderDate;
		private $FraudAlertCount;
		private $LastOrderDate;
		private $PaymentIncidentCount;
		private $RefusedManyTimesOrderCount;
		private $UnvalidatedOrderCount;
		private $ValidatedOneTimeOrderCount;
		private $ValidatedOrderCount;
		private $ClientIpAddressRecurrence;
		private $OngoingLitigationOrderAmount;
		private $PaidLitigationOrderAmount24Month;
		private $ScoreSimulationCount7Days;


		/**
		 * Class Constructor
		 * @param    $CanceledOrderAmount   
		 * @param    $CanceledOrderCount   
		 * @param    $FirstOrderDate   
		 * @param    $FraudAlertCount   
		 * @param    $LastOrderDate   
		 * @param    $PaymentIncidentCount   
		 * @param    $RefusedManyTimesOrderCount   
		 * @param    $UnvalidatedOrderCount   
		 * @param    $ValidatedOneTimeOrderCount   
		 * @param    $ValidatedOrderCount   
		 * @param    $ClientIpAddressRecurrence   
		 * @param    $OngoingLitigationOrderAmount   
		 * @param    $PaidLitigationOrderAmount24Month   
		 * @param    $ScoreSimulationCount7Days   
		 */
		public function __construct($CanceledOrderAmount, $CanceledOrderCount, $FirstOrderDate, $FraudAlertCount, $LastOrderDate, $PaymentIncidentCount, $RefusedManyTimesOrderCount, $UnvalidatedOrderCount, $ValidatedOneTimeOrderCount, $ValidatedOrderCount, $ClientIpAddressRecurrence, $OngoingLitigationOrderAmount, $PaidLitigationOrderAmount24Month, $ScoreSimulationCount7Days)
		{
			$this->CanceledOrderAmount = $CanceledOrderAmount;
			$this->CanceledOrderCount = $CanceledOrderCount;
			$this->FirstOrderDate = $FirstOrderDate;
			$this->FraudAlertCount = $FraudAlertCount;
			$this->LastOrderDate = $LastOrderDate;
			$this->PaymentIncidentCount = $PaymentIncidentCount;
			$this->RefusedManyTimesOrderCount = $RefusedManyTimesOrderCount;
			$this->UnvalidatedOrderCount = $UnvalidatedOrderCount;
			$this->ValidatedOneTimeOrderCount = $ValidatedOneTimeOrderCount;
			$this->ValidatedOrderCount = $ValidatedOrderCount;
			$this->ClientIpAddressRecurrence = $ClientIpAddressRecurrence;
			$this->OngoingLitigationOrderAmount = $OngoingLitigationOrderAmount;
			$this->PaidLitigationOrderAmount24Month = $PaidLitigationOrderAmount24Month;
			$this->ScoreSimulationCount7Days = $ScoreSimulationCount7Days;
		}
		
		public function exposeData()
		{
			if (is_object($this)){
				$tmp = get_object_vars($this);
			}

			if (is_array($tmp)) {
				$new = array();
				foreach ($tmp as $key => $value) {
					if (is_object($value)) {
						$new[$key] = $value->exposeData();
					}
					else{
						$new[$key] = $value;
					}
				}
				return $new;
			}
			else{
				return $tmp;
			}
		}
	
	    /**
	     * @return mixed
	     */
	    public function getCanceledOrderAmount()
	    {
	        return $this->CanceledOrderAmount;
	    }

	    /**
	     * @param mixed $CanceledOrderAmount
	     *
	     * @return self
	     */
	    public function setCanceledOrderAmount($CanceledOrderAmount)
	    {
	        $this->CanceledOrderAmount = $CanceledOrderAmount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCanceledOrderCount()
	    {
	        return $this->CanceledOrderCount;
	    }

	    /**
	     * @param mixed $CanceledOrderCount
	     *
	     * @return self
	     */
	    public function setCanceledOrderCount($CanceledOrderCount)
	    {
	        $this->CanceledOrderCount = $CanceledOrderCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getFirstOrderDate()
	    {
	        return $this->FirstOrderDate;
	    }

	    /**
	     * @param mixed $FirstOrderDate
	     *
	     * @return self
	     */
	    public function setFirstOrderDate($FirstOrderDate)
	    {
	        $this->FirstOrderDate = $FirstOrderDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getFraudAlertCount()
	    {
	        return $this->FraudAlertCount;
	    }

	    /**
	     * @param mixed $FraudAlertCount
	     *
	     * @return self
	     */
	    public function setFraudAlertCount($FraudAlertCount)
	    {
	        $this->FraudAlertCount = $FraudAlertCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getLastOrderDate()
	    {
	        return $this->LastOrderDate;
	    }

	    /**
	     * @param mixed $LastOrderDate
	     *
	     * @return self
	     */
	    public function setLastOrderDate($LastOrderDate)
	    {
	        $this->LastOrderDate = $LastOrderDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentIncidentCount()
	    {
	        return $this->PaymentIncidentCount;
	    }

	    /**
	     * @param mixed $PaymentIncidentCount
	     *
	     * @return self
	     */
	    public function setPaymentIncidentCount($PaymentIncidentCount)
	    {
	        $this->PaymentIncidentCount = $PaymentIncidentCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRefusedManyTimesOrderCount()
	    {
	        return $this->RefusedManyTimesOrderCount;
	    }

	    /**
	     * @param mixed $RefusedManyTimesOrderCount
	     *
	     * @return self
	     */
	    public function setRefusedManyTimesOrderCount($RefusedManyTimesOrderCount)
	    {
	        $this->RefusedManyTimesOrderCount = $RefusedManyTimesOrderCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getUnvalidatedOrderCount()
	    {
	        return $this->UnvalidatedOrderCount;
	    }

	    /**
	     * @param mixed $UnvalidatedOrderCount
	     *
	     * @return self
	     */
	    public function setUnvalidatedOrderCount($UnvalidatedOrderCount)
	    {
	        $this->UnvalidatedOrderCount = $UnvalidatedOrderCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getValidatedOneTimeOrderCount()
	    {
	        return $this->ValidatedOneTimeOrderCount;
	    }

	    /**
	     * @param mixed $ValidatedOneTimeOrderCount
	     *
	     * @return self
	     */
	    public function setValidatedOneTimeOrderCount($ValidatedOneTimeOrderCount)
	    {
	        $this->ValidatedOneTimeOrderCount = $ValidatedOneTimeOrderCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getValidatedOrderCount()
	    {
	        return $this->ValidatedOrderCount;
	    }

	    /**
	     * @param mixed $ValidatedOrderCount
	     *
	     * @return self
	     */
	    public function setValidatedOrderCount($ValidatedOrderCount)
	    {
	        $this->ValidatedOrderCount = $ValidatedOrderCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getClientIpAddressRecurrence()
	    {
	        return $this->ClientIpAddressRecurrence;
	    }

	    /**
	     * @param mixed $ClientIpAddressRecurrence
	     *
	     * @return self
	     */
	    public function setClientIpAddressRecurrence($ClientIpAddressRecurrence)
	    {
	        $this->ClientIpAddressRecurrence = $ClientIpAddressRecurrence;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOngoingLitigationOrderAmount()
	    {
	        return $this->OngoingLitigationOrderAmount;
	    }

	    /**
	     * @param mixed $OngoingLitigationOrderAmount
	     *
	     * @return self
	     */
	    public function setOngoingLitigationOrderAmount($OngoingLitigationOrderAmount)
	    {
	        $this->OngoingLitigationOrderAmount = $OngoingLitigationOrderAmount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaidLitigationOrderAmount24Month()
	    {
	        return $this->PaidLitigationOrderAmount24Month;
	    }

	    /**
	     * @param mixed $PaidLitigationOrderAmount24Month
	     *
	     * @return self
	     */
	    public function setPaidLitigationOrderAmount24Month($PaidLitigationOrderAmount24Month)
	    {
	        $this->PaidLitigationOrderAmount24Month = $PaidLitigationOrderAmount24Month;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getScoreSimulationCount7Days()
	    {
	        return $this->ScoreSimulationCount7Days;
	    }

	    /**
	     * @param mixed $ScoreSimulationCount7Days
	     *
	     * @return self
	     */
	    public function setScoreSimulationCount7Days($ScoreSimulationCount7Days)
	    {
	        $this->ScoreSimulationCount7Days = $ScoreSimulationCount7Days;

	        return $this;
	    }
	}
?>