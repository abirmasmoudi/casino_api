<?php 
	/**
	* 
	*/
	class StayDetails
	{
		private $Company;
		private $Destination;
		private $NightNumber;
		private $RoomRange;


		/**
		 * Class Constructor
		 * @param    $Company   
		 * @param    $Destination   
		 * @param    $NightNumber   
		 * @param    $RoomRange   
		 */
		public function __construct($Company, $Destination, $NightNumber, $RoomRange)
		{
			$this->Company = $Company;
			$this->Destination = $Destination;
			$this->NightNumber = $NightNumber;
			$this->RoomRange = $RoomRange;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getCompany()
	    {
	        return $this->Company;
	    }

	    /**
	     * @param mixed $Company
	     *
	     * @return self
	     */
	    public function setCompany($Company)
	    {
	        $this->Company = $Company;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getDestination()
	    {
	        return $this->Destination;
	    }

	    /**
	     * @param mixed $Destination
	     *
	     * @return self
	     */
	    public function setDestination($Destination)
	    {
	        $this->Destination = $Destination;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getNightNumber()
	    {
	        return $this->NightNumber;
	    }

	    /**
	     * @param mixed $NightNumber
	     *
	     * @return self
	     */
	    public function setNightNumber($NightNumber)
	    {
	        $this->NightNumber = $NightNumber;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRoomRange()
	    {
	        return $this->RoomRange;
	    }

	    /**
	     * @param mixed $RoomRange
	     *
	     * @return self
	     */
	    public function setRoomRange($RoomRange)
	    {
	        $this->RoomRange = $RoomRange;

	        return $this;
	    }
	}
?>