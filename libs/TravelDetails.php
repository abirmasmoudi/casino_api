<?php 
	/**
	* 
	*/
	class TravelDetails
	{
		private $Insurance;
		private $Type;
		private $DepartureDate;
		private $ReturnDate;
		private $DestinationCountry;
		private $TicketCount;
		private $TravellerCount;
		private $Class;
		private $OwnTicket;
		private $MainDepartureCompany;
		private $TravellerPassportList;
		private $DepartureAirport;
		private $ArrivalAirport;
		private $DiscountCode;
		private $LuggageSupplement;
		private $ModificationAnnulation;


		/**
		 * Class Constructor
		 * @param    $Insurance   
		 * @param    $Type   
		 * @param    $DepartureDate   
		 * @param    $ReturnDate   
		 * @param    $DestinationCountry   
		 * @param    $TicketCount   
		 * @param    $TravellerCount   
		 * @param    $Class   
		 * @param    $OwnTicket   
		 * @param    $MainDepartureCompany   
		 * @param    $TravellerPassportList   
		 * @param    $DepartureAirport   
		 * @param    $ArrivalAirport   
		 * @param    $DiscountCode   
		 * @param    $LuggageSupplement   
		 * @param    $ModificationAnnulation   
		 */
		public function __construct($Insurance, $Type, $DepartureDate, $ReturnDate, $DestinationCountry, $TicketCount, $TravellerCount, $Class, $OwnTicket, $MainDepartureCompany, $TravellerPassportList, $DepartureAirport, $ArrivalAirport, $DiscountCode, $LuggageSupplement, $ModificationAnnulation)
		{
			$this->Insurance = $Insurance;
			$this->Type = $Type;
			$this->DepartureDate = $DepartureDate;
			$this->ReturnDate = $ReturnDate;
			$this->DestinationCountry = $DestinationCountry;
			$this->TicketCount = $TicketCount;
			$this->TravellerCount = $TravellerCount;
			$this->Class = $Class;
			$this->OwnTicket = $OwnTicket;
			$this->MainDepartureCompany = $MainDepartureCompany;
			$this->TravellerPassportList = $TravellerPassportList;
			$this->DepartureAirport = $DepartureAirport;
			$this->ArrivalAirport = $ArrivalAirport;
			$this->DiscountCode = $DiscountCode;
			$this->LuggageSupplement = $LuggageSupplement;
			$this->ModificationAnnulation = $ModificationAnnulation;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
	
	    /**
	     * @return mixed
	     */
	    public function getInsurance()
	    {
	        return $this->Insurance;
	    }

	    /**
	     * @param mixed $Insurance
	     *
	     * @return self
	     */
	    public function setInsurance($Insurance)
	    {
	        $this->Insurance = $Insurance;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getType()
	    {
	        return $this->Type;
	    }

	    /**
	     * @param mixed $Type
	     *
	     * @return self
	     */
	    public function setType($Type)
	    {
	        $this->Type = $Type;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getDepartureDate()
	    {
	        return $this->DepartureDate;
	    }

	    /**
	     * @param mixed $DepartureDate
	     *
	     * @return self
	     */
	    public function setDepartureDate($DepartureDate)
	    {
	        $this->DepartureDate = $DepartureDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getReturnDate()
	    {
	        return $this->ReturnDate;
	    }

	    /**
	     * @param mixed $ReturnDate
	     *
	     * @return self
	     */
	    public function setReturnDate($ReturnDate)
	    {
	        $this->ReturnDate = $ReturnDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getDestinationCountry()
	    {
	        return $this->DestinationCountry;
	    }

	    /**
	     * @param mixed $DestinationCountry
	     *
	     * @return self
	     */
	    public function setDestinationCountry($DestinationCountry)
	    {
	        $this->DestinationCountry = $DestinationCountry;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTicketCount()
	    {
	        return $this->TicketCount;
	    }

	    /**
	     * @param mixed $TicketCount
	     *
	     * @return self
	     */
	    public function setTicketCount($TicketCount)
	    {
	        $this->TicketCount = $TicketCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTravellerCount()
	    {
	        return $this->TravellerCount;
	    }

	    /**
	     * @param mixed $TravellerCount
	     *
	     * @return self
	     */
	    public function setTravellerCount($TravellerCount)
	    {
	        $this->TravellerCount = $TravellerCount;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getClass()
	    {
	        return $this->Class;
	    }

	    /**
	     * @param mixed $Class
	     *
	     * @return self
	     */
	    public function setClass($Class)
	    {
	        $this->Class = $Class;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOwnTicket()
	    {
	        return $this->OwnTicket;
	    }

	    /**
	     * @param mixed $OwnTicket
	     *
	     * @return self
	     */
	    public function setOwnTicket($OwnTicket)
	    {
	        $this->OwnTicket = $OwnTicket;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMainDepartureCompany()
	    {
	        return $this->MainDepartureCompany;
	    }

	    /**
	     * @param mixed $MainDepartureCompany
	     *
	     * @return self
	     */
	    public function setMainDepartureCompany($MainDepartureCompany)
	    {
	        $this->MainDepartureCompany = $MainDepartureCompany;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTravellerPassportList()
	    {
	        return $this->TravellerPassportList;
	    }

	    /**
	     * @param mixed $TravellerPassportList
	     *
	     * @return self
	     */
	    public function setTravellerPassportList($TravellerPassportList)
	    {
	        $this->TravellerPassportList = $TravellerPassportList;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getDepartureAirport()
	    {
	        return $this->DepartureAirport;
	    }

	    /**
	     * @param mixed $DepartureAirport
	     *
	     * @return self
	     */
	    public function setDepartureAirport($DepartureAirport)
	    {
	        $this->DepartureAirport = $DepartureAirport;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getArrivalAirport()
	    {
	        return $this->ArrivalAirport;
	    }

	    /**
	     * @param mixed $ArrivalAirport
	     *
	     * @return self
	     */
	    public function setArrivalAirport($ArrivalAirport)
	    {
	        $this->ArrivalAirport = $ArrivalAirport;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getDiscountCode()
	    {
	        return $this->DiscountCode;
	    }

	    /**
	     * @param mixed $DiscountCode
	     *
	     * @return self
	     */
	    public function setDiscountCode($DiscountCode)
	    {
	        $this->DiscountCode = $DiscountCode;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getLuggageSupplement()
	    {
	        return $this->LuggageSupplement;
	    }

	    /**
	     * @param mixed $LuggageSupplement
	     *
	     * @return self
	     */
	    public function setLuggageSupplement($LuggageSupplement)
	    {
	        $this->LuggageSupplement = $LuggageSupplement;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getModificationAnnulation()
	    {
	        return $this->ModificationAnnulation;
	    }

	    /**
	     * @param mixed $ModificationAnnulation
	     *
	     * @return self
	     */
	    public function setModificationAnnulation($ModificationAnnulation)
	    {
	        $this->ModificationAnnulation = $ModificationAnnulation;

	        return $this;
	    }
	}
?>