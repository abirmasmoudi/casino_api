<?php
	/**
	* 
	*/
	class PaymentCardData
	{
		private $CardType;
		private $ExpirationDate;
		private $HolderBirthDate;
		private $Number;
		private $SecurityNumber;
		private $CardOptionId;
		private $StoredCardID;
		private $CardLabel;


		/**
		 * Class Constructor
		 * @param    $CardType   
		 * @param    $ExpirationDate   
		 * @param    $HolderBirthDate   
		 * @param    $Number   
		 * @param    $SecurityNumber   
		 * @param    $CardOptionId   
		 * @param    $StoredCardID   
		 * @param    $CardLabel   
		 */
		public function __construct($CardType, $ExpirationDate, $HolderBirthDate, $Number, $SecurityNumber, $CardOptionId, $StoredCardID, $CardLabel)
		{
			$this->CardType = $CardType;
			$this->ExpirationDate = $ExpirationDate;
			$this->HolderBirthDate = $HolderBirthDate;
			$this->Number = $Number;
			$this->SecurityNumber = $SecurityNumber;
			$this->CardOptionId = $CardOptionId;
			$this->StoredCardID = $StoredCardID;
			$this->CardLabel = $CardLabel;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getCardType()
	    {
	        return $this->CardType;
	    }

	    /**
	     * @param mixed $CardType
	     *
	     * @return self
	     */
	    public function setCardType($CardType)
	    {
	        $this->CardType = $CardType;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getExpirationDate()
	    {
	        return $this->ExpirationDate;
	    }

	    /**
	     * @param mixed $ExpirationDate
	     *
	     * @return self
	     */
	    public function setExpirationDate($ExpirationDate)
	    {
	        $this->ExpirationDate = $ExpirationDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getHolderBirthDate()
	    {
	        return $this->HolderBirthDate;
	    }

	    /**
	     * @param mixed $HolderBirthDate
	     *
	     * @return self
	     */
	    public function setHolderBirthDate($HolderBirthDate)
	    {
	        $this->HolderBirthDate = $HolderBirthDate;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getNumber()
	    {
	        return $this->Number;
	    }

	    /**
	     * @param mixed $Number
	     *
	     * @return self
	     */
	    public function setNumber($Number)
	    {
	        $this->Number = $Number;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getSecurityNumber()
	    {
	        return $this->SecurityNumber;
	    }

	    /**
	     * @param mixed $SecurityNumber
	     *
	     * @return self
	     */
	    public function setSecurityNumber($SecurityNumber)
	    {
	        $this->SecurityNumber = $SecurityNumber;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCardOptionId()
	    {
	        return $this->CardOptionId;
	    }

	    /**
	     * @param mixed $CardOptionId
	     *
	     * @return self
	     */
	    public function setCardOptionId($CardOptionId)
	    {
	        $this->CardOptionId = $CardOptionId;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getStoredCardID()
	    {
	        return $this->StoredCardID;
	    }

	    /**
	     * @param mixed $StoredCardID
	     *
	     * @return self
	     */
	    public function setStoredCardID($StoredCardID)
	    {
	        $this->StoredCardID = $StoredCardID;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCardLabel()
	    {
	        return $this->CardLabel;
	    }

	    /**
	     * @param mixed $CardLabel
	     *
	     * @return self
	     */
	    public function setCardLabel($CardLabel)
	    {
	        $this->CardLabel = $CardLabel;

	        return $this;
	    }
	}
?>