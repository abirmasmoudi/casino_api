<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>Index</title>
    <link rel="icon" type="image/png" sizes="32x32" href="https://www.banque-casino.fr/favicon-32x32.png">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
    <style>
        .not-active {
            pointer-events: none;
            cursor: default;
            text-decoration: none;
            color: black;
        }
    </style>
</head>
<body style="font-size: smaller">

    <div>
        <dl class="dl-horizontal">
            <dd>
                <a id="newOrder-button" class="ajaxButton" data-next="sts" href="">Generate order</a>
            </dd>
            <dt>Sans OneClick</dt>
            <dd>
            	<a id="sts-button" class="ajaxButton not-active" href="">Sts</a>
                
            	<a id="score-button" class="ajaxButton not-active" href="">Score</a>
                
                <!--PagePaiement-->
            	<a id="pagePaiement-button" class="ajaxButton not-active" href="">PagePaiement</a>
                <!--EndPagePaiement-->
                
                
            	<a id="updateOrder-button" class="ajaxButton not-active" href="">UpdateOrder</a>
            	<a id="GetOrderSchedule-button" class="ajaxButton not-active" href="">GetSchedule</a>
            </dd>
            
        </dl>
    </div>
    <div id="newOrder"></div>
    <div id="enrollementCarte">
        <div id="sts"></div>
        <div id="prescore"></div>
        <div id="score"></div>
        <div id="authorizeCardPayment"></div>
        <div id="pagePaiement"></div>
        <div id="checkCard3DEnrollment"></div>
        <div id="payOrderRank"></div>
        <div id="updateOrder"></div>
        <div id="GetOrderSchedule"></div>
    </div>
    <div id="OneClick">
        <div id="prescoreOneClick"></div>
        <div id="scoreOneClick"></div>
        <div id="authorizeCardPaymentOneClick"></div>
        <div id="pagePaiementOneClick"></div>
        <div id="checkCard3DEnrollmentOneClick"></div>
        <div id="payOrderRankOneClick"></div>
        <div id="updateOrderOneClick"></div>
        <div id="GetOrderScheduleOneClick"></div>
        <div id="getCustomerStoredCardListOneClick"></div>
        <div id="deleteStoredCardOneClick"></div>
    </div>


</body>
<script>
    window.addEventListener("message", function(event) {
        console.log("Hello from " + event.data);
        if (event.data == "notOneClick") {
            $("#checkCard3DEnrollment-button").next().removeClass("not-active");
        }
        if (event.data == "OneClick") {
            $("#checkCard3DEnrollmentOneClick-button").next().removeClass("not-active");
        }
    });
    
	$(document).ready(function()
	{   
	    $('.ajaxButton').click(function(e)
	    {
	    	e.preventDefault();
            var arg = e.target.id.split("-")[0];
            if (arg == "newOrder") {
                $(e.target).addClass("not-active");
                $('#' + $(e.target).data("next") + "-button").removeClass("not-active");
            }
            else{
                if (arg == "GetOrderSchedule") {
                    console.log("toto: " + $("#OneClick-button").length);
                    $(e.target).addClass("not-active");
                    if ($("#OneClick-button").length > 0) {
                        $('#newOrder-button').data("next", $("#OneClick div").first().attr("id"));
                        $('#newOrder-button').removeClass("not-active");
                    }
                }
                else{
                    if(arg == "checkCard3DEnrollment" || arg == "checkCard3DEnrollmentOneClick"){
                        $(e.target).addClass("not-active");
                    }
                    else if (arg == "pagePaiement") {
                        $(e.target).addClass("not-active");
                        $(e.target).next().removeClass("not-active");
                    }
                    else{
                        $(e.target).addClass("not-active");
                        $(e.target).next().removeClass("not-active");
                    }
                }
            }

	    	console.log(arg);
	        $.ajax(
	            {
	                url: './index.php?action=' + arg,
	                datatype: 'html',
	                type: 'GET',
	                success: function(data)
	                {
	                    $('#' + arg).empty();
	                    $('#' + arg).html(data);
	                }
	            });
	    });
	});
</script>
</html>
