<!DOCTYPE html> <!-- GOT html -->
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>New Girl</title>
		<link rel="stylesheet" type="text/css"  href="Vue/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css"  href="Vue/css/CSSenplus.css">
    	<link rel="stylesheet" type="text/css"  href="Vue/css/CSSPerso.css">

	</head>

	<header>
		<div class="row">
			<div class="container-fluid col-md-12 col-lg-12 col-lg-offset-0">
				
				<nav class="navbar navbar-inverse">
					<div class="container-fluid">
					   
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a class="navbar-brand" href="#">Connexion</a>
					    </div>

					   
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav navbar-right">
					        <li ><a href="index.php">Home </a></li>
					        <li class="active">
					       		<?php
					            	if(ControlAdmin::isAdmin()){
					            		echo '<li><a href="index.php?action=seDeconnecter">Deconnexion</a></li>
					            		<li class="dropdown">
								            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Action Administrateur<span class="caret"></span></a>
								            <ul class="dropdown-menu" role="menu">
									            <li><a href="index.php?action=pageAjoutAdmin">Ajouter un administrateur</a></li>
									            <li class="divider"></li>
									            <li><a href="index.php?action=pageSuppressionAdmin">Supprimer un administrateur</a></li>
								          	</ul>
							        	</li>';
					            	}
					            	else{
					            		echo '<li><a href="index.php?action=seConnecter">Connexion</a></li>';
					            	}
								?>
							</li>
					        
					      </ul>
					  
					     
					    </div>
					  </div>
					</nav>
				    	
			</div>

			<div class="container col-lg-12 col-lg-offset-5">
				<h1> New Girl </h1>
			</div>
		</div>

	</header>
	

	<body class="body" >


    	<div class="container">

			<div class="row background">

				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 col-lg-offset-0" id="first" >

					<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
					<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

			        <div class="col-lg-offset-3 col-md-10">
				            <?php 
				            	foreach ($tabErreur as $value) {
				            ?>
				            	<div class="alert alert-warning">

							        
							        <strong>Warning!</strong> <?php echo $value?>

							    </div>

				            <?php	
				            	}
				            ?>
			        </div>
    
				</div>
				
			</div>
		</div>
	</body>

	<footer>
		<div id="footer">
		    <div class="container">
		        <div class="row">
		            <h3 class="footertext">About Us:</h3>
		            <br>
		              <div class="col-md-4">
		                <center>
		                  <img src="vue/images/develop.png" class="img-circle" alt="the-brains">
		                  <br>
		                  <h4 class="footertext">Programmer</h4>
		                  <p class="footertext">Nicolas Martin (G3), étudiant en deuxième année de l'IUT Informatique de Clermont-Ferrand. <br> E-mail : nicolas.martin3@etu.udamail.fr<br>
		                </center>
		              </div>
		              <div class="col-md-4">
		                <center>
		                  <img src="vue/images/design.png" class="img-circle" alt="...">
		                  <br>
		                  <h4 class="footertext">Designer</h4>
		                  <p class="footertext">Nicolas Martin (G3), étudiant en deuxième année de l'IUT Informatique de Clermont-Ferrand. <br> E-mail : nicolas.martin3@etu.udamail.fr <br><br> Pierre-Antoine Sujobert (G3), étudiant en deuxième année de l'IUT Informatique de Clermont-Ferrand. <br> E-mail : pierre-antoine.sujobert@etu.udamail.fr<br>
		                </center>
		              </div>
		              <div class="col-md-4">
		                <center>
		                  <img src="vue/images/develop.png" class="img-circle" alt="...">
		                  <br>
		                  <h4 class="footertext">Programmer</h4>
		                  <p class="footertext">Pierre-Antoine Sujobert (G3), étudiant en deuxième année de l'IUT Informatique de Clermont-Ferrand. <br> E-mail : pierre-antoine.sujobert@etu.udamail.fr<br>
		                </center>
		              </div>
		              
		            </div>
		            <div class="row">
		            <p><center> <p class="footertext">Copyright 2015</p></center></p>
		        </div>
		    </div>
		</div>
	</footer>
	
</html>

