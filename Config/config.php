<?php

$username="";
$password="";
$host="";
$dbname="";

$dsn="mysql:host=$host;dbname=$dbname";

define("USERNAME",$username);
define("PASSWORD",$password);
define("DSN",$dsn);


// Vues : 

$vues['index']='Vue/index.php';
$vues['newOrder']='Vue/newOrder.php';
$vues['sts']='Vue/sts.php';
$vues['prescore']='Vue/prescore.php';
$vues['score']='Vue/score.php';
$vues['authorizeCardPayment']='Vue/authorizeCardPayment.php';
$vues['pagePaiement']='Vue/pagePaiement.php';
$vues['checkCard3DEnrollment']='Vue/checkCard3DEnrollment.php';
$vues['authorizeCard3DPayment']='Vue/authorizeCard3DPayment.php';
$vues['payOrderRank']='Vue/payOrderRank.php';
$vues['updateOrder']='Vue/updateOrder.php';
$vues['GetOrderSchedule']='Vue/GetOrderSchedule.php';
$vues['AuthorizeCardPaymentOneClick']='Vue/AuthorizeCardPaymentOneClick.php';
$vues['CheckCard3DEnrollmentOneClick']='Vue/CheckCard3DEnrollmentOneClick.php';
$vues['AuthorizeCard3DPayment']='Vue/AuthorizeCard3DPayment.php';
$vues['GetCustomerStoredCardList']='Vue/GetCustomerStoredCardList.php';
$vues['DeleteStoredCard']='Vue/DeleteStoredCard.php';
?>