<?php

	Class ControlUser{

		public function __construct($action){
			try{				
				$action=Validation::sanitize($action,gettype($action));
				switch ($action) {
					case 'index':
						$this->index();
						break;

					case 'newOrder':
						$this->newOrder();
						break;

					case 'sts':
						$this->sts();
						break;

					case 'prescore':
						$this->prescore();
						break;

					case 'prescoreOneClick':
						$this->prescore();
						break;

					case 'score':
						$this->score();
						break;

					case 'scoreOneClick':
						$this->score();
						break;
					
					case 'authorizeCardPayment':
						$this->authorizeCardPayment();
						break;

					case 'pagePaiement':
						$this->pagePaiement();
						break;

					case 'retourPagePaiement':
						$this->retourPagePaiement();
						break;

					case 'checkCard3DEnrollment':
						$this->checkCard3DEnrollment();
						break;

					case 'authorizeCard3DPayment':
						$this->authorizeCard3DPayment();
						break;

					case 'authorizeCard3DPaymentOneClick':
						$this->authorizeCard3DPaymentOneClick();
						break;

					case 'payOrderRank':
						$this->payOrderRank();
						break;

					case 'payOrderRankOneClick':
						$this->payOrderRank();
						break;

					case 'updateOrder':
						$this->updateOrder();
						break;

					case 'GetOrderSchedule':
						$this->GetOrderSchedule();
						break;

					case 'GetOrderScheduleOneClick':
						$this->GetOrderSchedule();
						break;

					case 'updateOrderOneClick':
						$this->updateOrder();
						break;

					case 'authorizeCardPaymentOneClick':
						$this->AuthorizeCardPaymentOneClick();
						break;

					case 'checkCard3DEnrollmentOneClick':
						$this->CheckCard3DEnrollmentOneClick();
						break;

					case 'AuthorizeCard3DPayment':
						$this->AuthorizeCard3DPayment();
						break;

					case 'getCustomerStoredCardListOneClick':
						$this->GetCustomerStoredCardList();
						break;

					case 'deleteStoredCardOneClick':
						$this->DeleteStoredCard();
						break;

					default:
						global $vues;
						$tabErreur[]="Action inconnue";
						require $vues['erreur'];
						break;
				}

			}
			catch(PDOException $e){
				throw new PDOException($e->getMessage());
				
			}
			catch(Exception $e){
				throw new Exception($e->getMessage());
			}
		}

		private function genererParam()
		{
			$entreprise = Entreprise::getInstance();
			/*AutomaticPayment*/
				/*Cb4xAuto*/
				$entreprise->setLogin("Cartegrise");
				$entreprise->setPassword("Cartegrise1234*");
				$entreprise->setRealm("https://paymentservices.recette-cb4x.fr/MerchantGatewayFrontService.svc");
				$entreprise->setTokenId(null);
				$entreprise->setMerchantId("38");
				$entreprise->setMerchantSiteId("7017");
				$entreprise->setLocalisation(new Localisation('FR','EUR','2','FR'));
				$entreprise->setKey(utf8_encode("336AC9E91CE394145B177CD14807D4F199A6AC74"));
				/*EndCb4xAuto*/
				
			/*EndAutomaticPayment*/

			
			$customer = Customer::getInstance();
			$customer->setCustomerRef("603");
			$customer->setLastName("Name");
			$customer->setFirstname("FirstName");
			$customer->setCivility("Mr");
			$customer->setMaidenName(null);
			$customer->setBirthDate("1978-10-25");
			$customer->setBirthZipCode("75019");
			$customer->setPhoneNumber("0550255878");
			$customer->setCellPhoneNumber("0685522369");
			$customer->setEmail("testuser@gmail.com");
			$customer->setAddress1("64 RUE TIQUETONNE");
			$customer->setAddress2(null);
			$customer->setAddress3(null);
			$customer->setAddress4(null);
			$customer->setZipCode("33300");
			$customer->setCity("PARIS");
			$customer->setCountry("FR");
			$customer->setNationality("FR");
			$customer->setIpAddress("127.0.0.1");
			$customer->setWhiteList("UNKNOWN");
			$customer->setCard(new PaymentCardData("MASTERCARD", (new DateTime("2020-10-30T00:00:00"))->format('c'), (new DateTime("1978-10-25T00:00:00"))->format('c'), "5017670000001800", "111", "0", null, null));

			
		}

		public function index(){
			global $vues;
			$this->genererParam();
			$entreprise = Entreprise::getInstance();
			$_SESSION['entreprise'] = $entreprise;
			$customer = Customer::getInstance();
			$_SESSION['customer'] = $customer;
			require $vues['index'];
		}

		public function newOrder(){
			Customer::setInstance($_SESSION['customer']);
			$customer = Customer::getInstance();
			$order = new Order(strval(date('c')), "DESKTOP", "TNT", "1", strval(rand()), strval(rand(12000,20000)), null, false);
			$customer->setOrder($order);

			

			

			/*ProductDetail*/
				$productDetails = new ProductDetails("Fonction", "2", "30");
				$customer->getOrder()->setProductDetails($productDetails);
			/*EndProductDetail*/

			/*ShippingDetail*/
				$ShippingDetails = new ShippingDetails("64 RUE TIQUETONNE", null, "PARIS", "France", "33300");
				$customer->getOrder()->setShippingDetails($ShippingDetails);
			/*EndShippingDetail*/

			$_SESSION['customer'] = $customer;
			echo '<pre>'; print_r($_SESSION); echo '</pre>';
			echo "OK !<br> --------------------- <br>";
			
		}

		public function sts(){
			global $vues;
			Entreprise::setInstance($_SESSION['entreprise']);
			$entreprise = Entreprise::getInstance();
			$login = new Login($entreprise);
            echo '---------- login -----------<pre>'; print_r($login); echo '</pre>';
			$entreprise = $login->connect();
            echo '<pre>'; print_r($entreprise); echo '</pre>';
			require $vues['sts'];
		}
		
		public function score(){
			global $vues;

			Entreprise::setInstance($_SESSION['entreprise']);
			$entreprise = Entreprise::getInstance();
			Customer::setInstance($_SESSION['customer']);
			$customer = Customer::getInstance();

			$score = new Score($entreprise, $customer);
			$score->mainScore();
		

		

		

		/*ProductDetailScore*/
			$score->ProductDetails(); //Optional
		/*EndProductDetailScore*/

		/*ShippingDetailsScore*/
			$score->ShippingDetails(); // Optional
		/*EndShippingDetailsScore*/

		

		/*AdditionalNumericFieldScore*/
			$score->AdditionalNumericFieldList(); //Optional
		/*EndAdditionalNumericFieldScore*/

		/*AdditionalStringListScore*/
			$score->AdditionalFieldOfstring(); //Optional
		/*EndAdditionalStringListScore*/

			$scoreResult = $score->executeMethod();


			$_SESSION['scoreResult'] = $scoreResult;
			require $vues['score'];
		}
		
		/*PagePaiement*/
		public function pagePaiement(){
			global $vues;
			Entreprise::setInstance($_SESSION['entreprise']);
			$entreprise = Entreprise::getInstance();
			Customer::setInstance($_SESSION['customer']);
			$customer = Customer::getInstance();
			ScoreResult::setInstance($_SESSION['scoreResult']);
			$scoreResult = ScoreResult::getInstance();
			
			$authorizeCardPayment = new AuthorizeCardPayment($entreprise, $customer, $scoreResult);
			$authorizeCardPayment->pagePaiement();
			//require $vues['pagePaiement'];
		}

		public function retourPagePaiement(){
			Customer::setInstance($_SESSION['customer']);
			$customer = Customer::getInstance();

			$customer->getCard()->setStoredCardID($_REQUEST['cardId']);
			$customer->getCard()->getCardLabel($_REQUEST['cardLabel']);
			$_SESSION['customer'] = $customer;
			return "ok";
		}
		/*EndPagePaiement*/
		
		
		public function updateOrder(){
			global $vues;
			Entreprise::setInstance($_SESSION['entreprise']);
			$entreprise = Entreprise::getInstance();
			Customer::setInstance($_SESSION['customer']);
			$customer = Customer::getInstance();
			ScoreResult::setInstance($_SESSION['scoreResult']);
			$scoreResult = ScoreResult::getInstance();

			$updateOrder = new UpdateOrder($entreprise, $customer, $scoreResult);
			$updateOrderResult = $updateOrder->updateOrder();
			require $vues['updateOrder'];
		}

		public function GetOrderSchedule(){
			global $vues;
			Entreprise::setInstance($_SESSION['entreprise']);
			$entreprise = Entreprise::getInstance();
			Customer::setInstance($_SESSION['customer']);
			$customer = Customer::getInstance();

			$getOrderSchedule = new GetOrderSchedule($entreprise, $customer);
			$getOrderScheduleResult = $getOrderSchedule->GetOrderSchedule();
			require $vues['GetOrderSchedule'];
		}
		
	}
?>