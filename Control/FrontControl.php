<?php
	class frontControl
	{
		private $listeAction_User = array('index','newOrder','sts','prescore','score','authorizeCardPayment','pagePaiement','checkCard3DEnrollment','authorizeCard3DPayment','payOrderRank','updateOrder','GetOrderSchedule','GetOrderScheduleOneClick','prescoreOneClick','scoreOneClick','authorizeCardPaymentOneClick','checkCard3DEnrollmentOneClick','AuthorizeCard3DPayment','authorizeCard3DPaymentOneClick','payOrderRankOneClick','updateOrderOneClick','getCustomerStoredCardListOneClick','deleteStoredCardOneClick','retourPagePaiement');

		public function __construct(){
			try{

				if(isset($_REQUEST['action'])){
					$action=$_REQUEST['action'];
					$action=Validation::sanitize($action,gettype($action));
				}
				else{
					$action='index';
				}

				switch ($action) {
					case in_array($action, $this->listeAction_User):
						$this->actionUtilisateur($action);
						break;

					default:
						$this->actionUtilisateur('erreur');
						break;
				}
			}
			catch(PDOException $e){
				global $vues;
				$tabErreur[]=$e->getMessage();
				require $vues['erreur'];
			}
			catch(Exception $e){
				global $vues;
				$tabErreur[]=$e->getMessage();
				require $vues['erreur'];
			}
		}
		
		public function actionUtilisateur($action){
			$controleUser = new ControlUser($action);
		}
	}