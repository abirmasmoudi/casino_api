<?php
	include_once './libs/Entreprise.php';
	include_once './libs/Util.php';

	/**
	* 
	*/
	class Login
	{
		public $wsdl;
		public $entreprise;

		public function __construct($entreprise)
		{
			//$this->wsdl = 'https://paymentsts.recette-cb4x.fr/Users/soapIssue.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_Issue.wsdl';
			$this->entreprise = $entreprise;
		}

		public function connect()
		{
		    try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("username"=>$this->entreprise->getLogin(),"password"=>$this->entreprise->getPassword(),"realm"=>$this->entreprise->getRealm());
		        $quates=$clinet->Issue($ver);

		        $array = Util::object_to_array($quates);

		        $token = $array['IssueResult']; //Token STS
		        
		        $this->entreprise->setTokenId($token);

		        return $this->entreprise;

		    }

		    catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
	}

	
?>