<?php
	include_once './libs/Util.php';
	/**
	* 
	*/
	class UpdateOrder
	{
		
		public $wsdl;
		public $entreprise;
		public $customer;
		public $tokenScore;

		public function __construct($entreprise, $customer, $scoreResult)
		{
			//$this->wsdl = 'https://paymentservices.recette-cb4x.fr/MerchantGatewayFrontService.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_MerchantGatewayFrontService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
			$this->tokenScore = $scoreResult->getScoringTokenResult();
		}

		public function updateOrder()
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("headerMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"updateOrderRequestMessage"=>array("NewAmount"=>"0",
		        											"OldAmount"=>strval($this->customer->getOrder()->getTotalAmount()),
		        											"OrderRef"=>$this->customer->getOrder()->getShoppingCartRef(),
		        											"OrderTag"=>null,
		        											"ScoringToken"=>$this->tokenScore));
		        $quates=$clinet->UpdateOrder($ver);

		        $array = Util::object_to_array($quates);

				$orderRefResult = $array['UpdateOrderResult']['OrderRef'];
				$orderTagResult = $array['UpdateOrderResult']['OrderTag'];
				$merchantIDResult = $array['UpdateOrderResult']['MerchantID'];
				$merchantSiteIDResult = $array['UpdateOrderResult']['MerchantSiteID'];
				$responseCodeResult = $array['UpdateOrderResult']['ResponseCode'];
				$responseMessageResult = $array['UpdateOrderResult']['ResponseMessage'];
				$scheduleResult = $array['UpdateOrderResult']['Schedule'];

				if ($responseCodeResult == "0") {
					return new UpdateOrderResult($orderRefResult, $orderTagResult, $merchantIDResult, $merchantSiteIDResult, $responseCodeResult, $responseMessageResult, $scheduleResult);
				}
				else{
					return null;
		        }


		    }

		    catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
	    
	}
?>