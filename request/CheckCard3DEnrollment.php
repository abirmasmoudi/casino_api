<?php
	include_once './libs/Util.php';
	/**
	* 
	*/
	class CheckCard3DEnrollment
	{
		
		public $wsdl;
		public $entreprise;
		public $customer;
		public $totalAmount;
		public $tokenScore;
		public function __construct($entreprise, $customer, $scoreResult)
		{
			//$this->wsdl = 'https://paymentservices.recette-cdiscount.com/CheckCard3DEnrollmentService.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_CheckCard3DEnrollmentService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
			$this->totalAmount = $scoreResult->getTotalAmountResult();
			$this->tokenScore = $scoreResult->getScoringTokenResult();
		}

		public function enrollement($returnUrl)
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("HeaderMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"CheckEnrollmentRequestMessage"=>array("AllowCardStorage"=>true,
			        												"CardData"=>array("CardOptionID"=>$this->customer->getCard()->getCardOptionId(),
			        																	"CardType"=>$this->customer->getCard()->getCardType(),
													        							"ExpirationDate"=>$this->customer->getCard()->getExpirationDate(),
													        							"HolderBirthDate"=>$this->customer->getCard()->getHolderBirthDate(),
													        							"Number"=>$this->customer->getCard()->getNumber(),
													        							"SecurityNumber"=>$this->customer->getCard()->getSecurityNumber(),
													        							"CardLabel"=>$this->customer->getCard()->getCardLabel()),
				        											"CustomerRef"=>"1",
				        											"InvoiceId"=>"0",
				        											"OrderDate"=>date('c'),
				        											"OrderRef"=>$this->customer->getOrder()->getShoppingCartRef(),
				        											/*Cb4x*/
																	"PaymentOptionRef"=>"63",
																	/*EndCb4x*/
																	
				        											"TotalAmount"=>$this->totalAmount,
				        											"NotificationUrl"=>null,
				        											"ReturnUrl"=>$returnUrl,
				        											"OrderTag"=>null,
				        											"ScoringToken"=>$this->tokenScore));
		        $quates=$clinet->CheckCard3DEnrollment($ver);

		        $array = Util::object_to_array($quates);

				$paymentResponseCodeResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseCode'];
				$merchantAccountRefResult = $array['CheckEnrollmentResponseMessage']['MerchantAccountRef'];
				$paymentResponseScheduleInfoResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseScheduleInfo'];
				$paymentResponseStoredCardResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseStoredCard'];
				$paymentResponseErrorMessageResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseErrorMessage'];
				$checkCard3DEnrollmentResponseCodeResult = $array['CheckEnrollmentResponseMessage']['CheckCard3DEnrollmentResponseCode'];
				$paymentRequestIDResult = $array['CheckEnrollmentResponseMessage']['PaymentRequestID'];
				$redirectionTypeResult = $array['CheckEnrollmentResponseMessage']['RedirectionType'];
				$redirectionUrlResult = $array['CheckEnrollmentResponseMessage']['RedirectionUrl'];
				$redirectionParamsResult = $array['CheckEnrollmentResponseMessage']['RedirectionParams'];



				if ($paymentResponseCodeResult == "Succeeded" && $checkCard3DEnrollmentResponseCodeResult == "Enrolled") {
					$checkCard3DEnrollmentResult = CheckCard3DEnrollmentResult::getInstance();
					$checkCard3DEnrollmentResult->paymentResponseCodeResult = $paymentResponseCodeResult;
					$checkCard3DEnrollmentResult->merchantAccountRefResult = $merchantAccountRefResult;
					$checkCard3DEnrollmentResult->paymentResponseScheduleInfoResult = $paymentResponseScheduleInfoResult;
					$checkCard3DEnrollmentResult->paymentResponseStoredCardResult = $paymentResponseStoredCardResult;
					$checkCard3DEnrollmentResult->paymentResponseErrorMessageResult = $paymentResponseErrorMessageResult;
					$checkCard3DEnrollmentResult->checkCard3DEnrollmentResponseCodeResult = $checkCard3DEnrollmentResponseCodeResult;
					$checkCard3DEnrollmentResult->paymentRequestIDResult = $paymentRequestIDResult;
					$checkCard3DEnrollmentResult->redirectionTypeResult = $redirectionTypeResult;
					$checkCard3DEnrollmentResult->redirectionUrlResult = $redirectionUrlResult;
					$checkCard3DEnrollmentResult->redirectionParamsResult = $redirectionParamsResult;

					return $checkCard3DEnrollmentResult;
				}
				else{
					return null;
		        }
			}

			catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
		/*OneClick*/
		public function oneClick($returnUrl)
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("HeaderMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"CheckEnrollmentRequestMessage"=>array("AllowCardStorage"=>true,
			        												"CardData"=>array("CardOptionID"=>$this->customer->getCard()->getCardOptionId(),
			        																	"CardType"=>$this->customer->getCard()->getCardType(),
													        							"ExpirationDate"=>$this->customer->getCard()->getExpirationDate(),
													        							"HolderBirthDate"=>$this->customer->getCard()->getHolderBirthDate(),
													        							"Number"=>$this->customer->getCard()->getNumber(),
													        							"SecurityNumber"=>$this->customer->getCard()->getSecurityNumber(),
													        							"StoredCardID"=>$this->customer->getCard()->getStoredCardID(),
													        							"CardLabel"=>$this->customer->getCard()->getCardLabel()),
				        											"CustomerRef"=>"1",
				        											"InvoiceId"=>"0",
				        											"OrderDate"=>date('c'),
				        											"OrderRef"=>$this->customer->getOrder()->getShoppingCartRef(),
				        											/*Cb4xOneClick*/
																	"PaymentOptionRef"=>"63",
																	/*EndCb4xOneClick*/
																	
				        											"TotalAmount"=>$this->totalAmount,
				        											"NotificationUrl"=>null,
				        											"ReturnUrl"=>$returnUrl,
				        											"OrderTag"=>null,
				        											"ScoringToken"=>$this->tokenScore,
				        											"PaymentAttempt"=>1,
				        											"FeesAmount"=>$this->totalAmount - $this->customer->getOrder()->getTotalAmount(),
				        											"ReportDelayInDays"=>0));
		        $quates=$clinet->CheckCard3DEnrollment($ver);

		        $array = Util::object_to_array($quates);

				$paymentResponseCodeResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseCode'];
				$merchantAccountRefResult = $array['CheckEnrollmentResponseMessage']['MerchantAccountRef'];
				$paymentResponseScheduleInfoResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseScheduleInfo'];
				$paymentResponseStoredCardResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseStoredCard'];
				$paymentResponseErrorMessageResult = $array['CheckEnrollmentResponseMessage']['PaymentResponseErrorMessage'];
				$checkCard3DEnrollmentResponseCodeResult = $array['CheckEnrollmentResponseMessage']['CheckCard3DEnrollmentResponseCode'];
				$paymentRequestIDResult = $array['CheckEnrollmentResponseMessage']['PaymentRequestID'];
				$redirectionTypeResult = $array['CheckEnrollmentResponseMessage']['RedirectionType'];
				$redirectionUrlResult = $array['CheckEnrollmentResponseMessage']['RedirectionUrl'];
				$redirectionParamsResult = $array['CheckEnrollmentResponseMessage']['RedirectionParams'];
				print "<pre>";
				print_r($ver);
				print "</pre>";
				print "<br>";
				print "<pre>";
				print_r($array);
				print "</pre>";

				if ($paymentResponseCodeResult == "Succeeded" && $checkCard3DEnrollmentResponseCodeResult == "Enrolled") {
					$checkCard3DEnrollmentResult = CheckCard3DEnrollmentResult::getInstance();
					$checkCard3DEnrollmentResult->paymentResponseCodeResult = $paymentResponseCodeResult;
					$checkCard3DEnrollmentResult->merchantAccountRefResult = $merchantAccountRefResult;
					$checkCard3DEnrollmentResult->paymentResponseScheduleInfoResult = $paymentResponseScheduleInfoResult;
					$checkCard3DEnrollmentResult->paymentResponseStoredCardResult = $paymentResponseStoredCardResult;
					$checkCard3DEnrollmentResult->paymentResponseErrorMessageResult = $paymentResponseErrorMessageResult;
					$checkCard3DEnrollmentResult->checkCard3DEnrollmentResponseCodeResult = $checkCard3DEnrollmentResponseCodeResult;
					$checkCard3DEnrollmentResult->paymentRequestIDResult = $paymentRequestIDResult;
					$checkCard3DEnrollmentResult->redirectionTypeResult = $redirectionTypeResult;
					$checkCard3DEnrollmentResult->redirectionUrlResult = $redirectionUrlResult;
					$checkCard3DEnrollmentResult->redirectionParamsResult = $redirectionParamsResult;

					return $checkCard3DEnrollmentResult;
				}
				else{
					return null;
		        }
			}

			catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
		/*EndOneClick*/
	}
?>