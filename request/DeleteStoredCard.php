<?php
	include_once './libs/Util.php';
	/**
	* 
	*/
	class DeleteStoredCard
	{
		
		public $wsdl;
		public $entreprise;
		public $customer;
		public $storedCard;
		public function __construct($entreprise, $customer, $storedCard)
		{
			//$this->wsdl = 'https://paymentservices.recette-cb4x.fr/StoredCardFrontService.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_StoredCardFrontService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
			$this->storedCard = $storedCard;
		}

		public function DeleteStoredCard()
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("headerMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"requestMessage"=>array("StoredCardID"=>"7237d0c85727437d862452a1b924373c"));
		        $quates=$clinet->DeleteStoredCard($ver);

		        $array = Util::object_to_array($quates);

				$responseCodeResult = $array['DeleteStoredCardResult']['ResponseCode'];
				$responseMessageResult = $array['DeleteStoredCardResult']['ResponseMessage'];
				$merchantIDResult = $array['DeleteStoredCardResult']['MerchantID'];
				$merchantSiteIDResult = $array['DeleteStoredCardResult']['MerchantSiteID'];
				$storedCardIDResult = $array['DeleteStoredCardResult']['StoredCardID'];


				if ($responseCodeResult == "0") {
					return new DeleteStoredCardResult($responseCodeResult, $responseMessageResult, $merchantIDResult, $merchantSiteIDResult, $storedCardIDResult);
				}
				else{
					return null;
		        }


		    }

		    catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
	}
?>