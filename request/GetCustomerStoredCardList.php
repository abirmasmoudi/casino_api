<?php
	include_once './libs/Util.php';
	/**
	* 
	*/
	class GetCustomerStoredCardList
	{
		
		public $wsdl;
		public $entreprise;
		public $customer;
		public function __construct($entreprise, $customer)
		{
			//$this->wsdl = 'https://paymentservices.recette-cb4x.fr/StoredCardFrontService.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_StoredCardFrontService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
		}

		public function GetCustomerStoredCardList()
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("headerMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"filterMessage"=>array("CustomerNumber"=>$this->customer->getCustomerRef()));
		        $quates=$clinet->GetCustomerStoredCardList($ver);

		        $array = Util::object_to_array($quates);

				$responseCodeResult = $array['GetCustomerStoredCardListResult']['ResponseCode'];
				$responseMessageResult = $array['GetCustomerStoredCardListResult']['ResponseMessage'];
				$customerNumberResult = $array['GetCustomerStoredCardListResult']['CustomerNumber'];
				$merchantIDResult = $array['GetCustomerStoredCardListResult']['MerchantID'];
				$merchantSiteIDResult = $array['GetCustomerStoredCardListResult']['MerchantSiteID'];
				$storedCardListResult = $array['GetCustomerStoredCardListResult']['StoredCardList'];


				if ($responseCodeResult == "0") {
					$getCustomerStoredCardListResult = GetCustomerStoredCardListResult::getInstance();
					$getCustomerStoredCardListResult->responseCodeResult = $responseCodeResult;
					$getCustomerStoredCardListResult->responseMessageResult = $responseMessageResult;
					$getCustomerStoredCardListResult->customerNumberResult = $customerNumberResult;
					$getCustomerStoredCardListResult->merchantIDResult = $merchantIDResult;
					$getCustomerStoredCardListResult->merchantSiteIDResult = $merchantSiteIDResult;
					$getCustomerStoredCardListResult->storedCardListResult = $storedCardListResult;

					return $getCustomerStoredCardListResult;
				}
				else{
					return null;
		        }


		    }

		    catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
	}
?>