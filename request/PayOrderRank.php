<?php
	include_once './libs/Util.php';
	/**
	* 
	*/
	class PayOrderRank
	{
		
		public $wsdl;
		public $entreprise;
		public $customer;
		
		public function __construct($entreprise, $customer)
		{
			//$this->wsdl = 'https://paymentservices.recette-cdiscount.com/MerchantGatewayFrontService.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_MerchantGatewayFrontService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
		}

		public function payOrderRank()
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("headerMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"payOrderRankRequestMessage"=>array("Amount"=>"0",
			        											"Attempt"=>"1",
			        											"OrderRef"=>$this->customer->getOrder()->getShoppingCartRef(),
			        											"Rank"=>"1"));
		        $quates=$clinet->PayOrderRank($ver);

		        $array = Util::object_to_array($quates);

				$merchantIDResult = $array['PayOrderRankResult']['MerchantID'];
				$merchantSiteIDResult = $array['PayOrderRankResult']['MerchantSiteID'];
				$orderRefResult = $array['PayOrderRankResult']['OrderRef'];
				$orderTagResult = $array['PayOrderRankResult']['OrderTag'];
				$rankResult = $array['PayOrderRankResult']['Rank'];
				$attemptResult = $array['PayOrderRankResult']['Attempt'];
				$amountResult = $array['PayOrderRankResult']['Amount'];
				$codeResult = $array['PayOrderRankResult']['Code'];
				$messageResult = $array['PayOrderRankResult']['Message'];
				$scheduleResult = $array['PayOrderRankResult']['Schedule'];

				if ($codeResult == "0") {
					return new PayOrderRankResult($merchantIDResult, $merchantSiteIDResult, $orderRefResult, $orderTagResult, $rankResult, $attemptResult, $amountResult, $codeResult, $messageResult, $scheduleResult);
				}
				else{
					return null;
		        }
			}

			catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
	}
?>