<?php
	

	/**
	* 
	*/
	class PreScore
	{
		public $wsdl;

		public $entreprise;
		public $customer;

		public $additionalField = 0;
		public $position = 0;

		public $clinet;
		public $ver;

		public function __construct($entreprise, $customer)
		{
			//$this->wsdl = 'https://services.recette-cb4x.fr/Cb4xFrontService.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_Cb4xFrontService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
			
		}

		public function executeMethod()
		{
			try{
				$quates=$this->clinet->PreScore($this->ver);

		        $array = get_object_vars($quates);
		        $array = Util::object_to_array($array['PreScoreResult']);
		        $responseCodeResult = $array['ResponseCode'];
		        $currencyResult = $array['Currency'];
		        $customerRefResult = $array['CustomerRef'];
		        $paymentAgreementResult = $array['PaymentAgreement'];
		        $requestIDResult = $array['RequestID'];
		        $shoppingCartRefResult = $array['ShoppingCartRef'];
		        $totalAmountResult = $array['TotalAmount'];

		        if (isset($array['PaymentScheduleList'])) {
					$scheduleResult = $array['PaymentScheduleList'];
		        }
		        else{
		        	$scheduleResult = null;
		        }

		        if ($responseCodeResult == "Success" && $paymentAgreementResult == true) {
		        	return new PreScoreResult($responseCodeResult, $currencyResult, $customerRefResult, $paymentAgreementResult, $requestIDResult, $shoppingCartRefResult, $totalAmountResult, $scheduleResult);
		        }
		        else{
		        	return null;
		        }
			}
			catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}

		public function mainPrescore()
		{
			try{
		        $this->clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
															    'trace' => 1,
															    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $this->ver = array("header"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        												"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
		        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
						        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
						        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
						        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
		        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
		        							"Version"=>"1"),
		        			"request"=>array("Customer"=>array("Address1"=>$this->customer->getAddress1(),
		        											"Address2"=>$this->customer->getAddress2(),
		        											"Address3"=>$this->customer->getAddress3(),
		        											"Adress4"=>$this->customer->getAddress4(),
						        							"BirthDate"=>$this->customer->getBirthDate(),
						        							"BirthZipCode"=>$this->customer->getBirthZipCode(),
						        							"CellPhoneNumber"=>$this->customer->getCellPhoneNumber(),
						        							"City"=>$this->customer->getCity(),
						        							"Civility"=>$this->customer->getCivility(),
						        							"Country"=>$this->customer->getCountry(),
						        							"CustomerRef"=>$this->customer->getCustomerRef(),
						        							"Email"=>$this->customer->getEmail(),
						        							"FirstName"=>$this->customer->getFirstName(),
						        							"LastName"=>$this->customer->getLastName(),
						        							"MaidenName"=>$this->customer->getMaidenName(),
						        							"PhoneNumber"=>$this->customer->getPhoneNumber(),
						        							"ZipCode"=>$this->customer->getZipCode(),
						        							"Nationality"=>$this->customer->getNationality(),
						        							"IpAddress"=>$this->customer->getIpAddress(),
						        							"WhiteList"=>$this->customer->getWhiteList()),
						        			"Order"=>array("OrderDate"=>$this->customer->getOrder()->getOrderDate(),
						        						"SaleChannel"=>$this->customer->getOrder()->getsaleChannel(),
						        						"ShippingMethod"=>$this->customer->getOrder()->getShippingMethod(),
						        						"ShoppingCartItemCount"=>$this->customer->getOrder()->getShoppingCartItemCount(),
						        						"ShoppingCartRef"=>$this->customer->getOrder()->getShoppingCartRef(),
						        						"TotalAmount"=>$this->customer->getOrder()->getTotalAmount())));
	        }

		    catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}

		public function CustomerHistory()
		{
			$this->ver["request"]["OptionalCustomerHistory"] = array("CanceledOrderAmount"=>$this->customer->getHistory()->getCanceledOrderAmount(),
					        											"CanceledOrderCount"=>$this->customer->getHistory()->getCanceledOrderCount(),
					        											"FirstOrderDate"=>$this->customer->getHistory()->getFirstOrderDate(),
					        											"FraudAlertCount"=>$this->customer->getHistory()->getFraudAlertCount(),
					        											"LastOrderDate"=>$this->customer->getHistory()->getLastOrderDate(),
					        											"PaymentIncidentCount"=>$this->customer->getHistory()->getPaymentIncidentCount(),
					        											"RefusedManyTimesOrderCount"=>$this->customer->getHistory()->getRefusedManyTimesOrderCount(),
					        											"UnvalidatedOrderCount"=>$this->customer->getHistory()->getUnvalidatedOrderCount(),
					        											"ValidatedOneTimeOrderCount"=>$this->customer->getHistory()->getValidatedOneTimeOrderCount(),
					        											"ValidatedOrderCount"=>$this->customer->getHistory()->getValidatedOrderCount(),
					        											"ClientIpAddressRecurrence"=>$this->customer->getHistory()->getClientIpAddressRecurrence(),
					        											"OngoingLitigationOrderAmount"=>$this->customer->getHistory()->getOngoingLitigationOrderAmount(),
					        											"PaidLitigationOrderAmount24Month"=>$this->customer->getHistory()->getPaidLitigationOrderAmount24Month(),
					        											"ScoreSimulationCount7Days"=>$this->customer->getHistory()->getScoreSimulationCount7Days());
		}

		public function TravelDetails()
		{
			$this->ver["request"]["OptionalTravelDetails"] = array("ArrivalAirport"=>$this->customer->getOrder()->getTravelDetails()->getArrivalAirport(),
				        											"Class"=>$this->customer->getOrder()->getTravelDetails()->getClass(),
				        											"DepartureAirport"=>$this->customer->getOrder()->getTravelDetails()->getDepartureAirport(),
				        											"DepartureDate"=>$this->customer->getOrder()->getTravelDetails()->getDepartureDate(),
				        											"DestinationCountry"=>$this->customer->getOrder()->getTravelDetails()->getDestinationCountry(),
				        											"Insurance"=>$this->customer->getOrder()->getTravelDetails()->getInsurance(),
				        											"MainDepartureCompany"=>$this->customer->getOrder()->getTravelDetails()->getMainDepartureCompany(),
				        											"OwnTicket"=>$this->customer->getOrder()->getTravelDetails()->getOwnTicket(),
				        											"ReturnDate"=>$this->customer->getOrder()->getTravelDetails()->getReturnDate(),
				        											"TicketCount"=>$this->customer->getOrder()->getTravelDetails()->getTicketCount(),
				        											"TravellerCount"=>$this->customer->getOrder()->getTravelDetails()->getTravellerCount(),
				        											"TravellerPassportList"=>$this->customer->getOrder()->getTravelDetails()->getTravellerPassportList(),
				        											"Type"=>$this->customer->getOrder()->getTravelDetails()->getType(),
				        											"DiscountCode"=>$this->customer->getOrder()->getTravelDetails()->getDiscountCode(),
				        											"LuggageSupplement"=>$this->customer->getOrder()->getTravelDetails()->getLuggageSupplement(),
				        											"ModificationAnnulation"=>$this->customer->getOrder()->getTravelDetails()->getModificationAnnulation());
		}

		public function StayDetails()
		{
			$this->ver["request"]["OptionalStayDetails"] = array("Company"=>$this->customer->getOrder()->getStayDetails()->getCompany(),
					        										"Destination"=>$this->customer->getOrder()->getStayDetails()->getDestination(),
					        										"NightNumber"=>$this->customer->getOrder()->getStayDetails()->getNightNumber(),
					        										"RoomRange"=>$this->customer->getOrder()->getStayDetails()->getRoomRange());
		}

		public function ProductDetails()
		{
			$this->ver["request"]["OptionalProductDetails"] = array("Categorie1"=>$this->customer->getOrder()->getProductDetails()->getCategorie1(),
																	"Categorie2"=>$this->customer->getOrder()->getProductDetails()->getCategorie2(),
																	"Categorie3"=>$this->customer->getOrder()->getProductDetails()->getCategorie3());
		}

		public function ShippingDetails()
		{
			$this->ver["request"]["OptionalShippingDetails"] = array("ShippingAdress1"=>$this->customer->getOrder()->getShippingDetails()->getAdress1(),
																	"ShippingAdress2"=>$this->customer->getOrder()->getShippingDetails()->getAdress2(),
																	"ShippingAdressCity"=>$this->customer->getOrder()->getShippingDetails()->getAdressCity(),
																	"ShippingAdressZip"=>$this->customer->getOrder()->getShippingDetails()->getAdressZip(),
																	"ShippingAdressCountry"=>$this->customer->getOrder()->getShippingDetails()->getAdressCountry());
		}

		public function PreScoreOptions()
		{
			$this->ver["request"]["PreScoreOption"] = array("ScheduleEnabled"=>strval($this->customer->getOrder()->getScheduleEnabled()));
		}

		public function AdditionalNumericFieldList()
		{
			for ($this->additionalField=0; $this->additionalField < 3; $this->additionalField++) { 
				$tmp["AdditionalFieldOfNullableOfint5F2dSckg"] = array("Index"=>$this->additionalField,
																		"Value"=>strval(rand(0,800000)));
				$this->ver["request"]["AdditionalNumericFieldList"][] = $tmp;
			}
			
			

			$this->position = $this->additionalField;
		}
		
		public function AdditionalFieldOfstring()
		{
			for ($this->additionalField=$this->position; $this->additionalField < $this->position + 3; $this->additionalField++) { 

				$tmp["AdditionalFieldOfstring"] = array("Index"=>$this->additionalField,
					        							"Value"=>"UNKNOW");
				$this->ver["request"]["AdditionalTextFieldList"][] = $tmp;
			}
			
			
		}
    }
?>