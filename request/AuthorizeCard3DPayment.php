<?php
	include_once './libs/Util.php';
	/**
	* 
	*/
	class AuthorizeCard3DPayment
	{
		
		public $wsdl;
		public $entreprise;
		public $customer;
		public $checkCard3DEnrollmentResult;
		public function __construct($entreprise, $customer, $checkCard3DEnrollmentResult)
		{
			//$this->$wsdl = 'https://paymentservices.recette-cdiscount.com/AuthorizeCard3DPayment.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_AuthorizeCard3DPayment.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
			$this->checkCard3DEnrollmentResult = $checkCard3DEnrollmentResult;
		}

		public function execute()
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
												    'trace' => 1,
												    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("headerMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"AuthorizeCard3DPaymentRequestMessage"=>array("OrderRef"=>$this->customer->getOrder()->getShoppingCartRef(),
					        											"OrderTag"=>null,
					        											"PaymentRequestID"=>$this->checkCard3DEnrollmentResult->getPaymentRequestIDResult()));
		        $quates=$clinet->AuthorizeCard3DPayment($ver);

		        $array = Util::object_to_array($quates);

				$paymentResponseCodeResult = $array['paymentResponseMessage']['PaymentResponseCode'];
				$merchantAccountRefResult = $array['paymentResponseMessage']['MerchantAccountRef'];
				$paymentResponseScheduleInfoResult = $array['paymentResponseMessage']['PaymentResponseScheduleInfo'];
				$paymentResponseStoredCardResult = $array['paymentResponseMessage']['PaymentResponseStoredCard'];
				$paymentResponseErrorMessageResult = $array['paymentResponseMessage']['PaymentResponseErrorMessage'];


				if ($paymentResponseCodeResult == "Succeeded") {
					return new AuthorizeCard3DPaymentResult($paymentResponseCodeResult, $merchantAccountRefResult, $paymentResponseScheduleInfoResult, $paymentResponseStoredCardResult, $paymentResponseErrorMessageResult);
				}
				else{
					return null;
		        }
			}

			catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
	}
?>