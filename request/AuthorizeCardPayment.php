<?php
	include_once './libs/Util.php';

	/**
	* 
	*/
	class AuthorizeCardPayment
	{
		public $wsdl;
		public $entreprise;
		public $customer;
		public $totalAmount;
		public $tokenScore;
		public function __construct($entreprise, $customer, $scoreResult)
		{
			$this->wsdl = './wsdl/RCT_PaymentProcessingService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
			$this->totalAmount = $scoreResult->getTotalAmountResult();
			$this->tokenScore = $scoreResult->getScoringTokenResult();
		}
		/*PaiementStandard*/
		public function paiement()
		{
			try{
		        $this->clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
															    'trace' => 1,
															    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $this->ver =array("headerMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"CardPaymentRequestMessage"=>array("CardData"=>array("CardType"=>$this->customer->getCard()->getCardType(),
											        							"ExpirationDate"=>$this->customer->getCard()->getExpirationDate(),
											        							"HolderBirthDate"=>$this->customer->getCard()->getHolderBirthDate(),
											        							"Number"=>$this->customer->getCard()->getNumber(),
											        							"SecurityNumber"=>$this->customer->getCard()->getSecurityNumber(),
											        							"CardLabel"=>$this->customer->getCard()->getCardLabel()),
			        											"CustomerRef"=>"1",
			        											"InvoiceId"=>"0",
			        											"OrderDate"=>date('Y-m-d'),
			        											"OrderRef"=>$this->customer->getOrder()->getShoppingCartRef(),
			        											/*Cb4x*/
																"PaymentOptionRef"=>"63",
																/*EndCb4x*/
																
			        											"TotalAmount"=>$this->totalAmount,
			        											"AllowCardStorage"=>true,
			        											"ScoringToken"=>$this->tokenScore));
		        $quates=$this->clinet->AuthorizeCardPayment($this->ver);


		        /*$array = get_object_vars($quates);
				$array = get_object_vars($array['ScoreResult']);

				$paymentResponseCodeResult = $array['PaymentResponseCode'];
				$merchantAccountRefResult = $array['MerchantAccountRef'];
				$paymentResponseScheduleInfoResult = $array['PaymentResponseScheduleInfo'];
				$paymentResponseStoredCardResult = get_object_vars($array['PaymentResponseStoredCard']);*/

				$array = Util::object_to_array($quates);

				$paymentResponseCodeResult = $array['paymentResponseMessage']['PaymentResponseCode'];
				$merchantAccountRefResult = $array['paymentResponseMessage']['MerchantAccountRef'];
				$paymentResponseScheduleInfoResult = $array['paymentResponseMessage']['PaymentResponseScheduleInfo'];
				$paymentResponseStoredCardResult = $array['paymentResponseMessage']['PaymentResponseStoredCard'];

				
				if ($paymentResponseCodeResult == "Succeeded") {
					$res =  new AuthorizeCardPaymentResult($paymentResponseCodeResult, $merchantAccountRefResult, $paymentResponseScheduleInfoResult, $paymentResponseStoredCardResult);
					/*if (isset($array['paymentResponseMessage']['PaymentResponseStoredCard'])) {
						$StoredCardIDResult = $array['paymentResponseMessage']['PaymentResponseStoredCard']['StoredCardID'];
						$StoredCardLabelResult = $array['paymentResponseMessage']['PaymentResponseStoredCard']['StoredCardLabel'];
						$res->setStoredCardID($StoredCardIDResult);
						$res->setStoredCardLabel($StoredCardLabelResult);
					}*/
					return $res;
				}
				else{
					return null;
		        }
			}

			catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
		/*EndPaiementStandard*/

	    /*PagePaiement*/
	    public function pagePaiement()
	    {
	    	$isOneClickActivated = false;
	    	
	    	$version = "1.0";
		    $merchantID = $this->entreprise->getMerchantId();
		    $merchantSiteID = $this->entreprise->getMerchantSiteId();
		
			/*Cb4x3D*/
			$paymentOptionRef = "63";
			/*EndCb4x3D*/
		

			
		    $orderRef = $this->customer->getOrder()->getShoppingCartRef();
		    $orderTag = "";
		    $freeText = "Coucou";
		    $decimalPosition = $this->entreprise->getLocalisation()->getDecimalPosition();
		    $currency = $this->entreprise->getLocalisation()->getCurrency();
		    $country = $this->entreprise->getLocalisation()->getCountry();
		    $invoiceID = "007";
		    $customerRef = "1";
		    $date = strval(date('Ymd'));
		    $amount = strval($this->totalAmount);
		    $orderRowsAmount = strval($this->customer->getOrder()->getTotalAmount());
		    $orderFeesAmount = strval($this->totalAmount - $this->customer->getOrder()->getTotalAmount());
		    $orderDiscountAmount = "1000";
		    $orderShippingCost = "3000";
		    $allowCardStorage ="";
		    $passwordRequired = "";
		    $merchantAuthenticateUrl = "";
		    $storedCardID1 = $this->customer->getCard()->getStoredCardID();//1..n
		    $storedCardLabel1 = $this->customer->getCard()->getCardLabel();//1..n
		    $merchantHomeUrl = "https://pageoriginal.com";
		    $merchantBackUrl = "https://pageoriginal.com/MerchantBack.aspx";
		    $merchantReturnUrl = "https://pageoriginal.com/MerchantReturn.aspx";
		    $merchantNotifyUrl = "";
		    $scoringToken = $this->tokenScore;

		    $concat = $version."*".$merchantID."*".$merchantSiteID."*".$paymentOptionRef."*".$orderRef;

		    if (!empty($orderTag)) {
		    	$concat = $concat."*".$orderTag;
		    }

		    if (!empty($freeText)) {
		    	$concat = $concat."*".$freeText;
		    }
		    else{
		    	$concat = $concat."*";
		    }

		    $concat = $concat."*".$decimalPosition."*".$currency."*".$country;

		    if (!empty($invoiceID)) {
		    	$concat = $concat."*".$invoiceID;
		    }
		    else{
		    	$concat = $concat."*";
		    }

		    $concat = $concat."*".$customerRef."*".$date."*".$amount."*".$orderRowsAmount."*".$orderFeesAmount."*".$orderDiscountAmount."*".$orderShippingCost;

		    if (!empty($allowCardStorage)) {
		    	$concat = $concat."*".$allowCardStorage;
		    }

		    if (!empty($passwordRequired)) {
		    	$concat = $concat."*".$passwordRequired;
		    }

		    if (!empty($merchantAuthenticateUrl)) {
		    	$concat = $concat."*".$merchantAuthenticateUrl;
		    }

		    if (!empty($storedCardID1)) {
		    	$concat = $concat."*".$storedCardID1;
		    }

		    if (!empty($storedCardLabel1)) {
		    	$concat = $concat."*".$storedCardLabel1;
		    }

		    $concat = $concat."*".$merchantHomeUrl;

		    if (!empty($merchantBackUrl)) {
		    	$concat = $concat."*".$merchantBackUrl;
		    }
		    else{
		    	$concat = $concat."*";
		    }

		    $concat = $concat."*".$merchantReturnUrl."*".$merchantNotifyUrl."*";

		    $hmac = hash_hmac('sha1', $concat, $this->entreprise->getKey(), false);
		    print_r($concat);
		    echo "<br>";
		    print_r($hmac);
		    echo "<form method=\"POST\" target=\"_blank\" name=\"PaymentForm\" action=\"https://payment.recette-cb4x.fr/V4/GenericRD/Redirect.aspx\">
		    		<input type=\"hidden\" name=\"version\" value=\"" . $version . "\"/>
					<input type=\"hidden\" name=\"merchantID\" value=\"" . $merchantID . "\"/>
					<input type=\"hidden\" name=\"merchantSiteID\" value=\"" . $merchantSiteID . "\"/>
					<input type=\"hidden\" name=\"paymentOptionRef\" value=\"" . $paymentOptionRef . "\"/>
					<input type=\"hidden\" name=\"orderRef\" value=\"" . $orderRef . "\"/>
					<input type=\"hidden\" name=\"orderTag\" value=\"" . $orderTag . "\"/>
					<input type=\"hidden\" name=\"freeText\" value=\"" . $freeText . "\"/>
					<input type=\"hidden\" name=\"decimalPosition\" value=\"" . $decimalPosition . "\"/>
					<input type=\"hidden\" name=\"currency\" value=\"" . $currency . "\"/>
					<input type=\"hidden\" name=\"country\" value=\"" . $country . "\"/>
					<input type=\"hidden\" name=\"invoiceID\" value=\"" . $invoiceID . "\"/>
					<input type=\"hidden\" name=\"customerRef\" value=\"" . $customerRef . "\"/>
					<input type=\"hidden\" name=\"date\" value=\"" . $date . "\"/>
					<input type=\"hidden\" name=\"amount\" value=\"" . $amount . "\"/>
					<input type=\"hidden\" name=\"orderRowsAmount\" value=\"" . $orderRowsAmount . "\"/>
					<input type=\"hidden\" name=\"orderFeesAmount\" value=\"" . $orderFeesAmount . "\"/>
					<input type=\"hidden\" name=\"orderDiscountAmount\" value=\"" . $orderDiscountAmount . "\"/>
					<input type=\"hidden\" name=\"orderShippingCost\" value=\"" . $orderShippingCost . "\"/>
					<input type=\"hidden\" name=\"allowCardStorage\" value=\"" . $allowCardStorage . "\"/>
					<input type=\"hidden\" name=\"passwordRequired\" value=\"" . $passwordRequired . "\"/>
					<input type=\"hidden\" name=\"merchantAuthenticateUrl\" value=\"" . $merchantAuthenticateUrl . "\"/>
					<input type=\"hidden\" name=\"storedCardID1\" value=\"" . $storedCardID1 . "\"/>
					<input type=\"hidden\" name=\"storedCardLabel1\" value=\"" . $storedCardLabel1 . "\"/>
					<input type=\"hidden\" name=\"merchantHomeUrl\" value=\"" . $merchantHomeUrl . "\"/>
					<input type=\"hidden\" name=\"merchantBackUrl\" value=\"" . $merchantBackUrl . "\"/>
					<input type=\"hidden\" name=\"merchantReturnUrl\" value=\"" . $merchantReturnUrl . "\"/>
					<input type=\"hidden\" name=\"merchantNotifyUrl\" value=\"" . $merchantNotifyUrl . "\"/>
					<input type=\"hidden\" name=\"scoringToken\" value=\"" . $scoringToken . "\"/>
					<input type=\"hidden\" name=\"hmac\" value=\"" . $hmac . "\"/>
					<input type=\"submit\" value=\"Envoyer le formulaire\">
		    	</form>";
		    	
				echo "<script>
						function suivant() {
							$(\"#updateOrder-button\").removeClass(\"not-active\");
							return true;
						}
					</script>";
				
		}
		/*EndPagePaiement*/
	}
?>

