<?php
	include_once './libs/Util.php';
	/**
	* 
	*/
	class GetOrderSchedule
	{
		
		public $wsdl;
		public $entreprise;
		public $customer;
		public function __construct($entreprise, $customer)
		{
			//$this->wsdl = 'https://paymentservices.recette-cb4x.fr/MerchantGatewayFrontService.svc?singleWsdl';
			$this->wsdl = './wsdl/RCT_MerchantGatewayFrontService.wsdl';
			$this->entreprise = $entreprise;
			$this->customer = $customer;
		}

		public function GetOrderSchedule()
		{
			try{
		        $clinet=new SoapClient($this->wsdl, array('soap_version'   => SOAP_1_1,  // use soap 1.1 client
													    'trace' => 1,
													    'stream_context' => stream_context_create(array('ssl' => array('crypto_method' =>  STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT)))));

		        $ver =array("headerMessage"=>array("Context"=>array("MerchantId"=>$this->entreprise->getMerchantId(),
		        													"MerchantSiteId"=>$this->entreprise->getMerchantSiteId()),
				        							"Localization"=>array("Country"=>$this->entreprise->getLocalisation()->getCountry(),
								        								"Currency"=>$this->entreprise->getLocalisation()->getCurrency(),
								        								"DecimalPosition"=>$this->entreprise->getLocalisation()->getDecimalPosition(),
								        								"Language"=>$this->entreprise->getLocalisation()->getLanguage()),
				        							"SecurityContext"=>array("TokenId"=>$this->entreprise->getTokenId()),
				        							"Version"=>"1"),
		        			"getOrderScheduleRequestMessage"=>array("OrderRef"=>$this->customer->getOrder()->getShoppingCartRef()));
		        $quates=$clinet->GetOrderSchedule($ver);

		        $array = Util::object_to_array($quates);

				$merchantIDResult = $array['GetOrderScheduleResult']['MerchantID'];
				$merchantSiteIDResult = $array['GetOrderScheduleResult']['MerchantSiteID'];
				$orderRefResult = $array['GetOrderScheduleResult']['OrderRef'];
				$orderTagResult = $array['GetOrderScheduleResult']['OrderTag'];
				$codeResult = $array['GetOrderScheduleResult']['Code'];
				$messageResult = $array['GetOrderScheduleResult']['Message'];
				$scheduleResult = $array['GetOrderScheduleResult']['Schedule'];


				if ($codeResult == "0") {
					return new GetOrderScheduleResult($merchantIDResult, $merchantSiteIDResult, $orderRefResult, $orderTagResult, $codeResult, $messageResult, $scheduleResult);
				}
				else{
					return null;
		        }


		    }

		    catch(SoapFault $e)
		    {
		        echo $e->getMessage();
		    }
		}
	}
?>