<?php
	/**
	* 
	*/
	class AuthorizeCardPaymentResult
	{
		private $PaymentResponseCode;
		private $MerchantAccountRef;
		private $PaymentResponseScheduleInfo;
		private $PaymentResponseStoredCard;


		/**
		 * Class Constructor
		 * @param    $PaymentResponseCode   
		 * @param    $MerchantAccountRef   
		 * @param    $PaymentResponseScheduleInfo   
		 * @param    $PaymentResponseStoredCard   
		 */
		public function __construct($PaymentResponseCode, $MerchantAccountRef, $PaymentResponseScheduleInfo, $PaymentResponseStoredCard)
		{
			$this->PaymentResponseCode = $PaymentResponseCode;
			$this->MerchantAccountRef = $MerchantAccountRef;
			$this->PaymentResponseScheduleInfo = $PaymentResponseScheduleInfo;
			$this->PaymentResponseStoredCard = $PaymentResponseStoredCard;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
	
	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseCode()
	    {
	        return $this->PaymentResponseCode;
	    }

	    /**
	     * @param mixed $PaymentResponseCode
	     *
	     * @return self
	     */
	    public function setPaymentResponseCode($PaymentResponseCode)
	    {
	        $this->PaymentResponseCode = $PaymentResponseCode;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantAccountRef()
	    {
	        return $this->MerchantAccountRef;
	    }

	    /**
	     * @param mixed $MerchantAccountRef
	     *
	     * @return self
	     */
	    public function setMerchantAccountRef($MerchantAccountRef)
	    {
	        $this->MerchantAccountRef = $MerchantAccountRef;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseScheduleInfo()
	    {
	        return $this->PaymentResponseScheduleInfo;
	    }

	    /**
	     * @param mixed $PaymentResponseScheduleInfo
	     *
	     * @return self
	     */
	    public function setPaymentResponseScheduleInfo($PaymentResponseScheduleInfo)
	    {
	        $this->PaymentResponseScheduleInfo = $PaymentResponseScheduleInfo;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseStoredCard()
	    {
	        return $this->PaymentResponseStoredCard;
	    }

	    /**
	     * @param mixed $PaymentResponseStoredCard
	     *
	     * @return self
	     */
	    public function setPaymentResponseStoredCard($PaymentResponseStoredCard)
	    {
	        $this->PaymentResponseStoredCard = $PaymentResponseStoredCard;

	        return $this;
	    }
	}
?>