<?php
	/**
	* 
	*/
	class AuthorizeCard3DPaymentResult
	{
		private $paymentResponseCodeResult;
		private $merchantAccountRefResult;
		private $paymentResponseScheduleInfoResult;
		private $paymentResponseStoredCardResult;
		private $paymentResponseErrorMessageResult;


		/**
		 * Class Constructor
		 * @param    $paymentResponseCodeResult   
		 * @param    $merchantAccountRefResult   
		 * @param    $paymentResponseScheduleInfoResult   
		 * @param    $paymentResponseStoredCardResult   
		 * @param    $paymentResponseErrorMessageResult   
		 */
		public function __construct($paymentResponseCodeResult, $merchantAccountRefResult, $paymentResponseScheduleInfoResult, $paymentResponseStoredCardResult, $paymentResponseErrorMessageResult)
		{
			$this->paymentResponseCodeResult = $paymentResponseCodeResult;
			$this->merchantAccountRefResult = $merchantAccountRefResult;
			$this->paymentResponseScheduleInfoResult = $paymentResponseScheduleInfoResult;
			$this->paymentResponseStoredCardResult = $paymentResponseStoredCardResult;
			$this->paymentResponseErrorMessageResult = $paymentResponseErrorMessageResult;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseCodeResult()
	    {
	        return $this->paymentResponseCodeResult;
	    }

	    /**
	     * @param mixed $paymentResponseCodeResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseCodeResult($paymentResponseCodeResult)
	    {
	        $this->paymentResponseCodeResult = $paymentResponseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantAccountRefResult()
	    {
	        return $this->merchantAccountRefResult;
	    }

	    /**
	     * @param mixed $merchantAccountRefResult
	     *
	     * @return self
	     */
	    public function setMerchantAccountRefResult($merchantAccountRefResult)
	    {
	        $this->merchantAccountRefResult = $merchantAccountRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseScheduleInfoResult()
	    {
	        return $this->paymentResponseScheduleInfoResult;
	    }

	    /**
	     * @param mixed $paymentResponseScheduleInfoResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseScheduleInfoResult($paymentResponseScheduleInfoResult)
	    {
	        $this->paymentResponseScheduleInfoResult = $paymentResponseScheduleInfoResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseStoredCardResult()
	    {
	        return $this->paymentResponseStoredCardResult;
	    }

	    /**
	     * @param mixed $paymentResponseStoredCardResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseStoredCardResult($paymentResponseStoredCardResult)
	    {
	        $this->paymentResponseStoredCardResult = $paymentResponseStoredCardResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseErrorMessageResult()
	    {
	        return $this->paymentResponseErrorMessageResult;
	    }

	    /**
	     * @param mixed $paymentResponseErrorMessageResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseErrorMessageResult($paymentResponseErrorMessageResult)
	    {
	        $this->paymentResponseErrorMessageResult = $paymentResponseErrorMessageResult;

	        return $this;
	    }
	}
?>