<?php
	/**
	* 
	*/
	class DeleteStoredCardResult
	{
		private $responseCodeResult;
		private $responseMessageResult;
		private $merchantIDResult;
		private $merchantSiteIDResult;
		private $storedCardIDResult;


		/**
		 * Class Constructor
		 * @param    $responseCodeResult   
		 * @param    $responseMessageResult   
		 * @param    $merchantIDResult   
		 * @param    $merchantSiteIDResult   
		 * @param    $storedCardIDResult   
		 */
		public function __construct($responseCodeResult, $responseMessageResult, $merchantIDResult, $merchantSiteIDResult, $storedCardIDResult)
		{
			$this->responseCodeResult = $responseCodeResult;
			$this->responseMessageResult = $responseMessageResult;
			$this->merchantIDResult = $merchantIDResult;
			$this->merchantSiteIDResult = $merchantSiteIDResult;
			$this->storedCardIDResult = $storedCardIDResult;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getResponseCodeResult()
	    {
	        return $this->responseCodeResult;
	    }

	    /**
	     * @param mixed $responseCodeResult
	     *
	     * @return self
	     */
	    public function setResponseCodeResult($responseCodeResult)
	    {
	        $this->responseCodeResult = $responseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getResponseMessageResult()
	    {
	        return $this->responseMessageResult;
	    }

	    /**
	     * @param mixed $responseMessageResult
	     *
	     * @return self
	     */
	    public function setResponseMessageResult($responseMessageResult)
	    {
	        $this->responseMessageResult = $responseMessageResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantIDResult()
	    {
	        return $this->merchantIDResult;
	    }

	    /**
	     * @param mixed $merchantIDResult
	     *
	     * @return self
	     */
	    public function setMerchantIDResult($merchantIDResult)
	    {
	        $this->merchantIDResult = $merchantIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantSiteIDResult()
	    {
	        return $this->merchantSiteIDResult;
	    }

	    /**
	     * @param mixed $merchantSiteIDResult
	     *
	     * @return self
	     */
	    public function setMerchantSiteIDResult($merchantSiteIDResult)
	    {
	        $this->merchantSiteIDResult = $merchantSiteIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getStoredCardIDResult()
	    {
	        return $this->storedCardIDResult;
	    }

	    /**
	     * @param mixed $storedCardIDResult
	     *
	     * @return self
	     */
	    public function setStoredCardIDResult($storedCardIDResult)
	    {
	        $this->storedCardIDResult = $storedCardIDResult;

	        return $this;
	    }
	}
?>