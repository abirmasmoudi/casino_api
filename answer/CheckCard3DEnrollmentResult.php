<?php
	/**
	* 
	*/
	class CheckCard3DEnrollmentResult
	{
		private static $_instance = null;
		private $paymentResponseCodeResult;
		private $merchantAccountRefResult;
		private $paymentResponseScheduleInfoResult;
		private $paymentResponseStoredCardResult;
		private $paymentResponseErrorMessageResult;
		private $checkCard3DEnrollmentResponseCodeResult;
		private $paymentRequestIDResult;
		private $redirectionTypeResult;
		private $redirectionUrlResult;
		private $redirectionParamsResult;

		public static function getInstance(){
			if (is_null(self::$_instance)) {
				self::$_instance = new CheckCard3DEnrollmentResult();
			}
			return self::$_instance;
		}

		public static function setInstance($instance){
			self::$_instance = $instance;
		}

		/**
		 * Class Constructor
		 * @param    $paymentResponseCodeResult   
		 * @param    $merchantAccountRefResult   
		 * @param    $paymentResponseScheduleInfoResult   
		 * @param    $paymentResponseStoredCardResult   
		 * @param    $paymentResponseErrorMessageResult   
		 * @param    $checkCard3DEnrollmentResponseCodeResult   
		 * @param    $paymentRequestIDResult   
		 * @param    $redirectionTypeResult   
		 * @param    $redirectionUrlResult   
		 * @param    $redirectionParamsResult   
		 */
		private function __construct()
		{
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseCodeResult()
	    {
	        return $this->paymentResponseCodeResult;
	    }

	    /**
	     * @param mixed $paymentResponseCodeResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseCodeResult($paymentResponseCodeResult)
	    {
	        $this->paymentResponseCodeResult = $paymentResponseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantAccountRefResult()
	    {
	        return $this->merchantAccountRefResult;
	    }

	    /**
	     * @param mixed $merchantAccountRefResult
	     *
	     * @return self
	     */
	    public function setMerchantAccountRefResult($merchantAccountRefResult)
	    {
	        $this->merchantAccountRefResult = $merchantAccountRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseScheduleInfoResult()
	    {
	        return $this->paymentResponseScheduleInfoResult;
	    }

	    /**
	     * @param mixed $paymentResponseScheduleInfoResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseScheduleInfoResult($paymentResponseScheduleInfoResult)
	    {
	        $this->paymentResponseScheduleInfoResult = $paymentResponseScheduleInfoResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseStoredCardResult()
	    {
	        return $this->paymentResponseStoredCardResult;
	    }

	    /**
	     * @param mixed $paymentResponseStoredCardResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseStoredCardResult($paymentResponseStoredCardResult)
	    {
	        $this->paymentResponseStoredCardResult = $paymentResponseStoredCardResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentResponseErrorMessageResult()
	    {
	        return $this->paymentResponseErrorMessageResult;
	    }

	    /**
	     * @param mixed $paymentResponseErrorMessageResult
	     *
	     * @return self
	     */
	    public function setPaymentResponseErrorMessageResult($paymentResponseErrorMessageResult)
	    {
	        $this->paymentResponseErrorMessageResult = $paymentResponseErrorMessageResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCheckCard3DEnrollmentResponseCodeResult()
	    {
	        return $this->checkCard3DEnrollmentResponseCodeResult;
	    }

	    /**
	     * @param mixed $checkCard3DEnrollmentResponseCodeResult
	     *
	     * @return self
	     */
	    public function setCheckCard3DEnrollmentResponseCodeResult($checkCard3DEnrollmentResponseCodeResult)
	    {
	        $this->checkCard3DEnrollmentResponseCodeResult = $checkCard3DEnrollmentResponseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentRequestIDResult()
	    {
	        return $this->paymentRequestIDResult;
	    }

	    /**
	     * @param mixed $paymentRequestIDResult
	     *
	     * @return self
	     */
	    public function setPaymentRequestIDResult($paymentRequestIDResult)
	    {
	        $this->paymentRequestIDResult = $paymentRequestIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRedirectionTypeResult()
	    {
	        return $this->redirectionTypeResult;
	    }

	    /**
	     * @param mixed $redirectionTypeResult
	     *
	     * @return self
	     */
	    public function setRedirectionTypeResult($redirectionTypeResult)
	    {
	        $this->redirectionTypeResult = $redirectionTypeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRedirectionUrlResult()
	    {
	        return $this->redirectionUrlResult;
	    }

	    /**
	     * @param mixed $redirectionUrlResult
	     *
	     * @return self
	     */
	    public function setRedirectionUrlResult($redirectionUrlResult)
	    {
	        $this->redirectionUrlResult = $redirectionUrlResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRedirectionParamsResult()
	    {
	        return $this->redirectionParamsResult;
	    }

	    /**
	     * @param mixed $redirectionParamsResult
	     *
	     * @return self
	     */
	    public function setRedirectionParamsResult($redirectionParamsResult)
	    {
	        $this->redirectionParamsResult = $redirectionParamsResult;

	        return $this;
	    }
	}
?>