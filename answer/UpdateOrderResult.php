<?php
	/**
	* 
	*/
	class UpdateOrderResult
	{
		private $orderRefResult;
		private $orderTagResult;
		private $merchantIDResult;
		private $merchantSiteIDResult;
		private $responseCodeResult;
		private $responseMessageResult;
		private $scheduleResult;


		/**
		 * Class Constructor
		 * @param    $orderRefResult   
		 * @param    $orderTagResult   
		 * @param    $merchantIDResult   
		 * @param    $merchantSiteIDResult   
		 * @param    $responseCodeResult   
		 * @param    $responseMessageResult   
		 * @param    $scheduleResult   
		 */
		public function __construct($orderRefResult, $orderTagResult, $merchantIDResult, $merchantSiteIDResult, $responseCodeResult, $responseMessageResult, $scheduleResult)
		{
			$this->orderRefResult = $orderRefResult;
			$this->orderTagResult = $orderTagResult;
			$this->merchantIDResult = $merchantIDResult;
			$this->merchantSiteIDResult = $merchantSiteIDResult;
			$this->responseCodeResult = $responseCodeResult;
			$this->responseMessageResult = $responseMessageResult;
			$this->scheduleResult = $scheduleResult;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getOrderRefResult()
	    {
	        return $this->orderRefResult;
	    }

	    /**
	     * @param mixed $orderRefResult
	     *
	     * @return self
	     */
	    public function setOrderRefResult($orderRefResult)
	    {
	        $this->orderRefResult = $orderRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOrderTagResult()
	    {
	        return $this->orderTagResult;
	    }

	    /**
	     * @param mixed $orderTagResult
	     *
	     * @return self
	     */
	    public function setOrderTagResult($orderTagResult)
	    {
	        $this->orderTagResult = $orderTagResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantIDResult()
	    {
	        return $this->merchantIDResult;
	    }

	    /**
	     * @param mixed $merchantIDResult
	     *
	     * @return self
	     */
	    public function setMerchantIDResult($merchantIDResult)
	    {
	        $this->merchantIDResult = $merchantIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantSiteIDResult()
	    {
	        return $this->merchantSiteIDResult;
	    }

	    /**
	     * @param mixed $merchantSiteIDResult
	     *
	     * @return self
	     */
	    public function setMerchantSiteIDResult($merchantSiteIDResult)
	    {
	        $this->merchantSiteIDResult = $merchantSiteIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getResponseCodeResult()
	    {
	        return $this->responseCodeResult;
	    }

	    /**
	     * @param mixed $responseCodeResult
	     *
	     * @return self
	     */
	    public function setResponseCodeResult($responseCodeResult)
	    {
	        $this->responseCodeResult = $responseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getResponseMessageResult()
	    {
	        return $this->responseMessageResult;
	    }

	    /**
	     * @param mixed $responseMessageResult
	     *
	     * @return self
	     */
	    public function setResponseMessageResult($responseMessageResult)
	    {
	        $this->responseMessageResult = $responseMessageResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getScheduleResult()
	    {
	        return $this->scheduleResult;
	    }

	    /**
	     * @param mixed $scheduleResult
	     *
	     * @return self
	     */
	    public function setScheduleResult($scheduleResult)
	    {
	        $this->scheduleResult = $scheduleResult;

	        return $this;
	    }
	}
?>