<?php
	/**
	* 
	*/
	class PayOrderRankResult
	{
		private $merchantIDResult;
		private $merchantSiteIDResult;
		private $orderRefResult;
		private $orderTagResult;
		private $rankResult;
		private $attemptResult;
		private $amountResult;
		private $codeResult;
		private $messageResult;
		private $scheduleResult;


		/**
		 * Class Constructor
		 * @param    $merchantIDResult   
		 * @param    $merchantSiteIDResult   
		 * @param    $orderRefResult   
		 * @param    $orderTagResult   
		 * @param    $rankResult   
		 * @param    $attemptResult   
		 * @param    $amountResult   
		 * @param    $codeResult   
		 * @param    $messageResult   
		 * @param    $scheduleResult   
		 */
		public function __construct($merchantIDResult, $merchantSiteIDResult, $orderRefResult, $orderTagResult, $rankResult, $attemptResult, $amountResult, $codeResult, $messageResult, $scheduleResult)
		{
			$this->merchantIDResult = $merchantIDResult;
			$this->merchantSiteIDResult = $merchantSiteIDResult;
			$this->orderRefResult = $orderRefResult;
			$this->orderTagResult = $orderTagResult;
			$this->rankResult = $rankResult;
			$this->attemptResult = $attemptResult;
			$this->amountResult = $amountResult;
			$this->codeResult = $codeResult;
			$this->messageResult = $messageResult;
			$this->scheduleResult = $scheduleResult;
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getMerchantIDResult()
	    {
	        return $this->merchantIDResult;
	    }

	    /**
	     * @param mixed $merchantIDResult
	     *
	     * @return self
	     */
	    public function setMerchantIDResult($merchantIDResult)
	    {
	        $this->merchantIDResult = $merchantIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantSiteIDResult()
	    {
	        return $this->merchantSiteIDResult;
	    }

	    /**
	     * @param mixed $merchantSiteIDResult
	     *
	     * @return self
	     */
	    public function setMerchantSiteIDResult($merchantSiteIDResult)
	    {
	        $this->merchantSiteIDResult = $merchantSiteIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOrderRefResult()
	    {
	        return $this->orderRefResult;
	    }

	    /**
	     * @param mixed $orderRefResult
	     *
	     * @return self
	     */
	    public function setOrderRefResult($orderRefResult)
	    {
	        $this->orderRefResult = $orderRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getOrderTagResult()
	    {
	        return $this->orderTagResult;
	    }

	    /**
	     * @param mixed $orderTagResult
	     *
	     * @return self
	     */
	    public function setOrderTagResult($orderTagResult)
	    {
	        $this->orderTagResult = $orderTagResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRankResult()
	    {
	        return $this->rankResult;
	    }

	    /**
	     * @param mixed $rankResult
	     *
	     * @return self
	     */
	    public function setRankResult($rankResult)
	    {
	        $this->rankResult = $rankResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAttemptResult()
	    {
	        return $this->attemptResult;
	    }

	    /**
	     * @param mixed $attemptResult
	     *
	     * @return self
	     */
	    public function setAttemptResult($attemptResult)
	    {
	        $this->attemptResult = $attemptResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getAmountResult()
	    {
	        return $this->amountResult;
	    }

	    /**
	     * @param mixed $amountResult
	     *
	     * @return self
	     */
	    public function setAmountResult($amountResult)
	    {
	        $this->amountResult = $amountResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCodeResult()
	    {
	        return $this->codeResult;
	    }

	    /**
	     * @param mixed $codeResult
	     *
	     * @return self
	     */
	    public function setCodeResult($codeResult)
	    {
	        $this->codeResult = $codeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMessageResult()
	    {
	        return $this->messageResult;
	    }

	    /**
	     * @param mixed $messageResult
	     *
	     * @return self
	     */
	    public function setMessageResult($messageResult)
	    {
	        $this->messageResult = $messageResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getScheduleResult()
	    {
	        return $this->scheduleResult;
	    }

	    /**
	     * @param mixed $scheduleResult
	     *
	     * @return self
	     */
	    public function setScheduleResult($scheduleResult)
	    {
	        $this->scheduleResult = $scheduleResult;

	        return $this;
	    }
	}
?>