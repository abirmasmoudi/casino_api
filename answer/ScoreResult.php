<?php
	/**
	* 
	*/
	class ScoreResult
	{
		private static $_instance = null;
		private $responseCodeResult;
		private $currencyResult;
		private $customerRefResult;
		private $paymentAgreementResult;
		private $paymentScheduleList;
		private $requestIDResult;
		private $ScoringTokenResult;
		private $shoppingCartRefResult;
		private $totalAmountResult;

		public static function getInstance(){
			if (is_null(self::$_instance)) {
				self::$_instance = new ScoreResult();
			}
			return self::$_instance;
		}

		public static function setInstance($instance){
			self::$_instance = $instance;
		}

		/**
		 * Class Constructor
		 * @param    $responseCodeResult   
		 * @param    $responseMessageResult   
		 * @param    $currencyResult   
		 * @param    $customerRefResult   
		 * @param    $paymentAgreementResult   
		 * @param    $paymentScheduleList   
		 * @param    $requestIDResult   
		 * @param    $ScoringTokenResult   
		 * @param    $shoppingCartRefResult   
		 * @param    $totalAmountResult   
		 */
		private function __construct()
		{
		}

		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getResponseCodeResult()
	    {
	        return $this->responseCodeResult;
	    }

	    /**
	     * @param mixed $responseCodeResult
	     *
	     * @return self
	     */
	    public function setResponseCodeResult($responseCodeResult)
	    {
	        $this->responseCodeResult = $responseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCurrencyResult()
	    {
	        return $this->currencyResult;
	    }

	    /**
	     * @param mixed $currencyResult
	     *
	     * @return self
	     */
	    public function setCurrencyResult($currencyResult)
	    {
	        $this->currencyResult = $currencyResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCustomerRefResult()
	    {
	        return $this->customerRefResult;
	    }

	    /**
	     * @param mixed $customerRefResult
	     *
	     * @return self
	     */
	    public function setCustomerRefResult($customerRefResult)
	    {
	        $this->customerRefResult = $customerRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentAgreementResult()
	    {
	        return $this->paymentAgreementResult;
	    }

	    /**
	     * @param mixed $paymentAgreementResult
	     *
	     * @return self
	     */
	    public function setPaymentAgreementResult($paymentAgreementResult)
	    {
	        $this->paymentAgreementResult = $paymentAgreementResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentScheduleList()
	    {
	        return $this->paymentScheduleList;
	    }

	    /**
	     * @param mixed $paymentScheduleList
	     *
	     * @return self
	     */
	    public function setPaymentScheduleList($paymentScheduleList)
	    {
	        $this->paymentScheduleList = $paymentScheduleList;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRequestIDResult()
	    {
	        return $this->requestIDResult;
	    }

	    /**
	     * @param mixed $requestIDResult
	     *
	     * @return self
	     */
	    public function setRequestIDResult($requestIDResult)
	    {
	        $this->requestIDResult = $requestIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getScoringTokenResult()
	    {
	        return $this->ScoringTokenResult;
	    }

	    /**
	     * @param mixed $ScoringTokenResult
	     *
	     * @return self
	     */
	    public function setScoringTokenResult($ScoringTokenResult)
	    {
	        $this->ScoringTokenResult = $ScoringTokenResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getShoppingCartRefResult()
	    {
	        return $this->shoppingCartRefResult;
	    }

	    /**
	     * @param mixed $shoppingCartRefResult
	     *
	     * @return self
	     */
	    public function setShoppingCartRefResult($shoppingCartRefResult)
	    {
	        $this->shoppingCartRefResult = $shoppingCartRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTotalAmountResult()
	    {
	        return $this->totalAmountResult;
	    }

	    /**
	     * @param mixed $totalAmountResult
	     *
	     * @return self
	     */
	    public function setTotalAmountResult($totalAmountResult)
	    {
	        $this->totalAmountResult = $totalAmountResult;

	        return $this;
	    }
	}
?>