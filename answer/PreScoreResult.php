<?php
	/**
	* 
	*/
	class PreScoreResult
	{
		private $responseCodeResult;
		private $currencyResult;
		private $customerRefResult;
		private $paymentAgreementResult;
		private $requestIDResult;
		private $shoppingCartRefResult;
		private $totalAmountResult;
		private $scheduleResult;


		/**
		 * Class Constructor
		 * @param    $responseCodeResult
		 * @param    $currencyResult   
		 * @param    $customerRefResult   
		 * @param    $paymentAgreementResult   
		 * @param    $requestIDResult   
		 * @param    $shoppingCartRefResult   
		 * @param    $totalAmountResult   
		 */
		public function __construct($responseCodeResult, $currencyResult, $customerRefResult, $paymentAgreementResult, $requestIDResult, $shoppingCartRefResult, $totalAmountResult,$scheduleResult)
		{
			$this->responseCodeResult = $responseCodeResult;
			$this->currencyResult = $currencyResult;
			$this->customerRefResult = $customerRefResult;
			$this->paymentAgreementResult = $paymentAgreementResult;
			$this->requestIDResult = $requestIDResult;
			$this->shoppingCartRefResult = $shoppingCartRefResult;
			$this->totalAmountResult = $totalAmountResult;
			$this->scheduleResult = $scheduleResult;
		}

		public function exposeData()
		{
			return get_object_vars($this);
		}

		
	    /**
	     * @return mixed
	     */
	    public function getResponseCodeResult()
	    {
	        return $this->responseCodeResult;
	    }

	    /**
	     * @param mixed $responseCodeResult
	     *
	     * @return self
	     */
	    public function setResponseCodeResult($responseCodeResult)
	    {
	        $this->responseCodeResult = $responseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCurrencyResult()
	    {
	        return $this->currencyResult;
	    }

	    /**
	     * @param mixed $currencyResult
	     *
	     * @return self
	     */
	    public function setCurrencyResult($currencyResult)
	    {
	        $this->currencyResult = $currencyResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCustomerRefResult()
	    {
	        return $this->customerRefResult;
	    }

	    /**
	     * @param mixed $customerRefResult
	     *
	     * @return self
	     */
	    public function setCustomerRefResult($customerRefResult)
	    {
	        $this->customerRefResult = $customerRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getPaymentAgreementResult()
	    {
	        return $this->paymentAgreementResult;
	    }

	    /**
	     * @param mixed $paymentAgreementResult
	     *
	     * @return self
	     */
	    public function setPaymentAgreementResult($paymentAgreementResult)
	    {
	        $this->paymentAgreementResult = $paymentAgreementResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getRequestIDResult()
	    {
	        return $this->requestIDResult;
	    }

	    /**
	     * @param mixed $requestIDResult
	     *
	     * @return self
	     */
	    public function setRequestIDResult($requestIDResult)
	    {
	        $this->requestIDResult = $requestIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getShoppingCartRefResult()
	    {
	        return $this->shoppingCartRefResult;
	    }

	    /**
	     * @param mixed $shoppingCartRefResult
	     *
	     * @return self
	     */
	    public function setShoppingCartRefResult($shoppingCartRefResult)
	    {
	        $this->shoppingCartRefResult = $shoppingCartRefResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getTotalAmountResult()
	    {
	        return $this->totalAmountResult;
	    }

	    /**
	     * @param mixed $totalAmountResult
	     *
	     * @return self
	     */
	    public function setTotalAmountResult($totalAmountResult)
	    {
	        $this->totalAmountResult = $totalAmountResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getScheduleResult()
	    {
	        return $this->scheduleResult;
	    }

	    /**
	     * @param mixed $scheduleResult
	     *
	     * @return self
	     */
	    public function setScheduleResult($scheduleResult)
	    {
	        $this->scheduleResult = $scheduleResult;

	        return $this;
	    }
	}
?>