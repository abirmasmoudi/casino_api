<?php
	/**
	* 
	*/
	class GetCustomerStoredCardListResult
	{
		private static $_instance = null;
		private $responseCodeResult;
		private $responseMessageResult;
		private $customerNumberResult;
		private $merchantIDResult;
		private $merchantSiteIDResult;
		private $storedCardListResult;

		public static function getInstance(){
			if (is_null(self::$_instance)) {
				self::$_instance = new GetCustomerStoredCardListResult();
			}
			return self::$_instance;
		}

		public static function setInstance($instance){
			self::$_instance = $instance;
		}

		/**
		 * Class Constructor
		 * @param    $responseCodeResult   
		 * @param    $responseMessageResult   
		 * @param    $customerNumberResult   
		 * @param    $merchantIDResult   
		 * @param    $merchantSiteIDResult   
		 * @param    $storedCardListResult   
		 */
		private function __construct()
		{
		}
		
		public function exposeData()
		{
			return get_object_vars($this);
		}
		
	    /**
	     * @return mixed
	     */
	    public function getResponseCodeResult()
	    {
	        return $this->responseCodeResult;
	    }

	    /**
	     * @param mixed $responseCodeResult
	     *
	     * @return self
	     */
	    public function setResponseCodeResult($responseCodeResult)
	    {
	        $this->responseCodeResult = $responseCodeResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getResponseMessageResult()
	    {
	        return $this->responseMessageResult;
	    }

	    /**
	     * @param mixed $responseMessageResult
	     *
	     * @return self
	     */
	    public function setResponseMessageResult($responseMessageResult)
	    {
	        $this->responseMessageResult = $responseMessageResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getCustomerNumberResult()
	    {
	        return $this->customerNumberResult;
	    }

	    /**
	     * @param mixed $customerNumberResult
	     *
	     * @return self
	     */
	    public function setCustomerNumberResult($customerNumberResult)
	    {
	        $this->customerNumberResult = $customerNumberResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantIDResult()
	    {
	        return $this->merchantIDResult;
	    }

	    /**
	     * @param mixed $merchantIDResult
	     *
	     * @return self
	     */
	    public function setMerchantIDResult($merchantIDResult)
	    {
	        $this->merchantIDResult = $merchantIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getMerchantSiteIDResult()
	    {
	        return $this->merchantSiteIDResult;
	    }

	    /**
	     * @param mixed $merchantSiteIDResult
	     *
	     * @return self
	     */
	    public function setMerchantSiteIDResult($merchantSiteIDResult)
	    {
	        $this->merchantSiteIDResult = $merchantSiteIDResult;

	        return $this;
	    }

	    /**
	     * @return mixed
	     */
	    public function getStoredCardListResult()
	    {
	        return $this->storedCardListResult;
	    }

	    /**
	     * @param mixed $storedCardListResult
	     *
	     * @return self
	     */
	    public function setStoredCardListResult($storedCardListResult)
	    {
	        $this->storedCardListResult = $storedCardListResult;

	        return $this;
	    }
	}
?>